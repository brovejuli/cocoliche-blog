<?php

// Define the webserver and path parameters
// * DIR_FS_* = Filesystem directories (local/physical)
// * DIR_WS_* = Webserver directories (virtual/URL)
define('HTTP_SERVER', 'http://cocoliche.dev/'); //http://www.centromedicosion.com/
define('HTTP_WWW', 'http://cocoliche.dev/'); // eg, http://localhost - should not be empty for productive servers
define('HTTP_WEBSITE', 'http://cocoliche.dev/'); // eg, http://localhost - should not be empty for productive servers
define('HTTP_GESTION', 'http://cocoliche.dev/gestion/'); // eg, http://localhost - should not be empty for productive servers
define('HTTPS_SERVER', ''); // eg, https://localhost - should not be empty for productive servers
define('ENABLE_SSL', false); // secure webserver for checkout procedure?
define('HTTP_COOKIE_DOMAIN', '');
define('HTTPS_COOKIE_DOMAIN', '');
define('HTTPS_COOKIE_PATH', '');
define('DIR_WS_HTTPS_CATALOG', '');
define('DIR_WS_IMAGES', HTTP_GESTION . 'images/');
define('DIR_WS_IMAGES_WEBSITE', HTTP_WEBSITE . 'images/');
define('DIR_WS_IMAGES_BLOG_THUMB', DIR_WS_IMAGES . 'blogmanagement/blog/thumb/');
define('DIR_WS_IMAGES_BLOG_BIG', DIR_WS_IMAGES . 'blogmanagement/blog/big/');
define('DIR_WS_CSS', HTTP_SERVER . 'css/');
define('DIR_WS_JS', HTTP_SERVER . 'js/');
define('DIR_WS_JS_PAGES', HTTP_SERVER . 'js/pages/');
define('DIR_WS_BOWER_COMPONENTS', HTTP_SERVER . 'vendor/bower_components/');
define('DIR_WS_COMMON', 'common/');
define('DIR_WS_INCLUDES', 'includes/');
define('DIR_WS_CLASS', 'class/');
define('TITLE', 'Gestion blogs - Cocoliche');
define('DIR_WS_POST_PREVIEW', HTTP_WEBSITE . 'detalle-blog.php');


// define our database connection
define('DB_SERVER', 'localhost'); // eg, localhost - should not be empty for productive servers
define('DB_SERVER_USERNAME', 'root'); //
define('DB_SERVER_PASSWORD', ''); //
define('DB_DATABASE', 'power_cocoliche'); //
define('USE_PCONNECT', 'false'); // use persistent connections?
define('STORE_SESSIONS', ''); // leave empty '' for default handler or set to 'mysql'
?>