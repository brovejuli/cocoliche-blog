<?php
define('__ROOTWEB__', dirname(dirname(__FILE__)));
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
$databaseManager = new DatabaseManager();
foreach ($databaseManager->databaseTables as $tables)
    // defino los nombres de las tablas de base de datos de este proyecto
    define('TABLE_' . strtoupper($tables[0]), $tables[0]);

?>