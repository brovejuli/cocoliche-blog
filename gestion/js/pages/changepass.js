$(document).ready(function (e) {
	
	$('input[name="password"]').focus();
	
	$( "#form-password" ).submit(function( event ) {
	 	$(".error").hide();
			var hasError = false;
			var pass = $("#password").val();
			var newpass = $("#password-new").val();
			var caracteres = $("#password-new").val().length;
			var checkVal = $("#password-check").val();
			if (pass == '') {
				$("#password").after('<span class="error">Por favor ingresar contrase&ntilde;a.</span>');
				hasError = true;
			}
			
			if (newpass == '') {							
				$("#password-new").after('<span class="error">Por favor ingresar nueva contrase&ntilde;a</span>');
				hasError = true;
			} else if (caracteres < 5) {
				$("#password-new").after('<span class="error">La contrase&ntilde;a debe ser mayor a cinco caracteres.</span>');
				hasError = true;
			} else if (checkVal == '') {
				$("#password-check").after('<span class="error">Por favor repetir nueva contrase&ntilde;a.</span>');
				hasError = true;
			} else if (newpass != checkVal ) {
				$("#password-check").after('<span class="error">Las contrase&ntilde;a no son iguales.</span>');
				hasError = true;
			}
	
			if(hasError == true) {return false;}
	
			if(hasError == false) {
					var srt = $("#form-password").serialize();
					$.ajax({
					type: "POST",
					url: "update.php",
					data: srt,
					dataType:"json",
					success: function(data){
							if (data.msg)
								alert (data.msg);
							else
								window.location = 'main.php?logout=salir';
						}
					});
			}
		return false;
	});


});

