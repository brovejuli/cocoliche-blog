var q = '';
var campo = '';
var srt = '';
var order = '';
var page = '1';
var url;

$(document).ready(function () {


    $('.fecha_input').datepicker({
        format: 'mm/dd/yyyy',
        language: 'en'
    });


    $("#q").focus();

    $('#q').bind('keypress', function (e) {
        if (e.which == 13) {
            listar();
        }
    });

    /*$("#frm").submit(function(event){
     // cancels the form submission
     event.preventDefault();
     doSave();
     });*/

    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {}
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();
        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                $("#modal_form").modal("hide");
                $('.data-table').bootstrapTable('refresh');
                $form.data('bootstrapValidator').resetForm();
            } else
                alertError(result.message);
        }, 'json');
    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew() {
    $('.modal-title').text('New User');
    $(".clearval").val('');
    url = 'new.php';
    return false;
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);
        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Confirm Delete ' + name);
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = 'action=users&id=' + id;
}

function doRemove() {

    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
            }
            else
                alertError(data.msg);

        }
    });
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}


$(function () {
    var $result = $('#eventsResult');

    $('.data-table').on('all.bs.table', function (e, name, args) {
        // console.log('Event:', name, ', data:', args);
    })
        .on('click-row.bs.table', function (e, row, $element) {
            $result.text('Event: click-row.bs.table');
        })
        .on('dbl-click-row.bs.table', function (e, row, $element) {
            $result.text('Event: dbl-click-row.bs.table');
        })
        .on('sort.bs.table', function (e, name, order) {
            $result.text('Event: sort.bs.table');
        })
        .on('check.bs.table', function (e, row) {
        })
        .on('uncheck.bs.table', function (e, row) {
            $result.text('Event: uncheck.bs.table');
        })
        .on('check-all.bs.table', function (e) {
            $result.text('Event: check-all.bs.table');
        })
        .on('uncheck-all.bs.table', function (e) {
            $result.text('Event: uncheck-all.bs.table');
        })
        .on('load-success.bs.table', function (e, data) {
            $result.text('Event: load-success.bs.table');
        })
        .on('load-error.bs.table', function (e, status) {
            $result.text('Event: load-error.bs.table');
        })
        .on('column-switch.bs.table', function (e, field, checked) {
            $result.text('Event: column-switch.bs.table');
        })
        .on('page-change.bs.table', function (e, number, size) {
            $result.text('Event: page-change.bs.table');
        })
        .on('search.bs.table', function (e, text) {
            $result.text('Event: search.bs.table');
        });
});

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar programa');
        url = 'update.php';
        $.each(row, function (key, value) {
            console.log(key + ' - ' + value);
            $('#frm #' + key).val(value);
        });
        $('#modal_form').modal('show');
    },

};