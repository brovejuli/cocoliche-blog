var q = '';
var campo = '';
var srt = '';
var order = '';
var page = '1';
var url;

$(document).ready(function () {

      $('.fecha_input').datepicker({
        format: 'mm/dd/yyyy',
        language: 'en'
    });


    $("#q").focus();

    $('#q').bind('keypress', function (e) {
        if (e.which == 13) {
            listar();
        }
    });

    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            category: {
                message: 'El nombre de la categoria no es valido',
                validators: {
                    notEmpty: {
                        message: 'El nombre de la categoria no puede estar vacio.'
                    },
                    stringLength: {
                        min: 3,
                        max: 15,
                        message: 'El nombre de la categoria debe tener entre 3 y 15 caracteres!'
                    },
                }
            },

        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                $("#modal_form").modal("hide");
                listar();
            }
        }, 'json');


    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doAlertRemove(id) {
    $(".modal-title").text('Eliminar email');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    $('#frmDelete').find('#Id').val(id);
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);
        }
    });
}


function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Elimiar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar el programa ' + row.program_type);
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.Id);
        console.log(value, row, index);
    }
};