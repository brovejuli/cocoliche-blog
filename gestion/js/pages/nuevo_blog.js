var url = 'new.php';
var carpeta_imagenes = 'images/blogmanagement/blog/';

$(document).ready(function () {

    autocompleteUserLevels();

    if (parseInt($('#update').val()))
        url = 'update.php'
    else
        url = 'new.php'

    if ($('#frm #Id').val())
        getBlogData($('#frm #Id').val())

    $('#fecha').datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true,
    });

    $('textarea').summernote({
        height: 200,
        toolbar: [
            ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['picture', 'video', 'link']],
        ],
        lang: 'es-ES',
    });

    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fecha: {
                validators: {
                    notEmpty: {
                        message: 'Campo requerido'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        swal_procesando();
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                window.location.href = "main.php?page=listar_blog&title=Blog";
            }
        }, 'json');
    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

    $('#subir').click(function () {
        var form_data = new FormData();
        form_data.append('imagen', $('#imagen').prop('files')[0]);
        form_data.append('carpeta', 'blog');
        swal({
            title: 'Procesando imagen...',
            text: '<span class="fa fa-refresh fa-spin fa-3x"></span>',
            html: true,
            type: 'warning',
            allowOutsideClick: false,
            showConfirmButton: false,
        })
        $.ajax({
            async: true,
            url: 'upload.php',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            beforeSend: function (response) {
                $('#subir').prop('disabled', true)
            },
            success: function (response) {
                swal.close()
                response = response.trim();
                $('.links').html('<div class="file-container"><div class="col-md-6 col-md-offset-3 text-center"><img class="img-thumbnail ' + (response.split('.')[0]).trim() + '"  src="' + carpeta_imagenes + 'thumb/' + response + '"><input type="hidden" name="archivo[]" value="' + response + '"><div class="clearfix"></div><a class="' + response + '" href="' + carpeta_imagenes + 'big/' + response + '" target="_blank">' + response + ' </a><div class="clearfix"></div><button class="btn btn-flat btn-primary rotate-image" data-imagen="' + response + '" data-grados="90" type="button"><span class="fa fa-undo"></span></button> <button class="btn btn-flat btn-primary rotate-image" data-imagen="' + response + '" data-grados="-90" type="button"><span class="fa fa-repeat"></span></button> <button type="button" class="btn btn-flat btn-danger btn-block delete-image"> <span class="fa fa-times"></span> Eliminar</button><br></div></div>')
                $('#imagen').val('')
                $('#file-trigger').text('Seleccionar archivo')
                $('#subir').prop('disabled', false)
            }
        });
    })
    $('#file-trigger').click(function () {
        $('#imagen').click()
    })
    $('#imagen').change(function () {
        $('#file-trigger').text($('#imagen').val().replace(/C:\\fakepath\\/i, ''))
    })
    $('.links')
        .on('click', '.delete-image', function () {
            $(this).parent().remove()
        })
        .on('click', '.rotate-image', function () {
            var imagen = $(this).data('imagen')
            swal({
                title: 'Procesando imagen...',
                text: '<span class="fa fa-refresh fa-spin fa-3x"></span>',
                html: true,
                type: 'warning',
                allowOutsideClick: false,
                showConfirmButton: false,
            })
            $.ajax({
                async: true,
                url: 'rotate.php',
                type: 'POST',
                data: {
                    imagen: $(this).data('imagen'),
                    grados: $(this).data('grados'),
                    carpeta: 'blog',
                },
                beforeSend: function (response) {
                },
                success: function (response) {
                    swal.close()
                    $('.links img.' + response.split('.')[0]).attr('src', carpeta_imagenes + 'thumb/' + imagen + "?v=" + (new Date()).getTime())
                }
            });
        })
});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew() {
    $('.modal-title').text('Nueva categoria');
    $(".clearval").val('');
    $('#frm #action').val($('#action').val());
    url = 'new.php';
    return false;
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Eliminar usuario');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = {
        action: 'user',
        Id: id,
    }
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doChangePass(id, name) {
    $(".modal-title-pass").text('Change Password ' + name);
    $('#modal_pass').modal('show');
    $('.clearval').val('')
    $('#frmPass input[name=Id]').val(id);
    url = 'update.php';
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}

function autocompleteUserLevels() {
    $.post('autocompletar.php', {action: 'user_levels'}, function (result) {
        if (result) {
            $('#level').html(result)
        }
    }, 'json');
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Eliminar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar categoria ' + row.category);
        url = 'update.php';
        $.each(row, function (key, value) {
            console.log(key + ' - ' + value);
            $('#frm #' + key).val(value);
        });
        $('#modal_form').modal('show');
    },

    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar la categoria ' + row.category);
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.Id);
        console.log(value, row, index);
    }
};
function getBlogData(Id) {
    $.ajax({
        async: true,
        url: 'listar.php',
        type: 'POST',
        data: {
            Id: Id,
            action: 'blog'
        },
        dataType: 'json',
        beforeSend: function (response) {
        },
        success: function (response) {
            $.each(response.blog, function (key, value) {
                if (key == 'cuerpo' || key == 'copete')
                    $("#" + key).summernote("code", value);
                else
                    $('#' + key).val(value)
            })
            $.each(response.imagenes, function (key, value) {
                $('.links').html('<div class="file-container"><div class="col-md-6 col-md-offset-3 text-center"><img class="img-thumbnail ' + (value.split('.')[0]).trim() + '"  src="' + carpeta_imagenes + 'thumb/' + value + '"><input type="hidden" name="archivo[]" value="' + value + '"><div class="clearfix"></div><a class="' + value + '" href="' + carpeta_imagenes + 'big/' + value + '" target="_blank">' + value + ' </a><div class="clearfix"></div><button class="btn btn-flat btn-primary rotate-image" data-imagen="' + value + '" data-grados="90" type="button"><span class="fa fa-undo"></span></button> <button class="btn btn-flat btn-primary rotate-image" data-imagen="' + value + '" data-grados="-90" type="button"><span class="fa fa-repeat"></span></button> <button type="button" class="btn btn-flat btn-danger btn-block delete-image"> <span class="fa fa-times"></span> Eliminar</button><br></div></div>')
            })
        }
    });
}