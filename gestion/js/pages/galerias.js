var q = '';
var campo = '';
var srt = '';
var order = '';
var page = '1';
var url;

$(document).ready(function () {

    autocompleteUserLevels();

    $('.fecha_input').datepicker({
        format: 'mm/dd/yyyy',
        language: 'en'
    });

    $(".email").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    });
    $('#filtro_obra').change(function () {
        $('.data-table').bootstrapTable('refresh');
    })


    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            category: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'The name is required and can\'t be empty'
                    }
                }
            },


        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                $("#modal_form").modal("hide");
                $('.data-table').bootstrapTable('refresh');
            }
        }, 'json');
    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew() {
    window.location.href = "main.php?page=fotos&title=Fotos";
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Eliminar usuario');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = {
        action: 'user',
        Id: id,
    }
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doChangePass(id, name) {
    $(".modal-title-pass").text('Change Password ' + name);
    $('#modal_pass').modal('show');
    $('.clearval').val('')
    $('#frmPass input[name=Id]').val(id);
    url = 'update.php';
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}

function autocompleteUserLevels() {
    $.post('autocompletar.php', {action: 'user_levels'}, function (result) {
        if (result) {
            $('#level').html(result)
        }
    }, 'json');
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones ',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="view"  href="javascript:void(0)"><i class="fa fa-eye"></i> Visualizar</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Elimiar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar categoria ' + row.category);
        url = 'update.php';
        $.each(row, function (key, value) {
            console.log(key + ' - ' + value);
            $('#frm #' + key).val(value);
        });
        $('#modal_form').modal('show');
    },
    'click .view': function (e, value, row, index) {
        window.open("../obra.php?obra=" + row.id_obra);
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar la imagen ?');
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.id_imagen);
        console.log(value, row, index);
    }
};

function queryParams() {
    var params = {};
    params["obra"] = $('#filtro_obra').val();
    return params;
}