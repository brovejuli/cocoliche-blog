var q = '';
var campo = '';
var srt = '';
var order = '';
var page = '1';
var url = 'new.php';

$(document).ready(function () {

    autocompleteUserLevels();
    /*Dropzone.autoDiscover = false;
     try {
     var myDropzone = new Dropzone("#dropzone", {
     paramName: "file", // The name that will be used to transfer the file
     maxFilesize: 3, // MB
     url: 'upload.php',
     addRemoveLinks: true,
     dictDefaultMessage: '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Arrastrar imagenes</span> para subir \
     <span class="smaller-80 grey">(o hacer click)</span> <br /> \
     <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>'
     ,
     dictResponseError: 'Error while uploading file!',

     //change the previewTemplate to use Bootstrap progress bars
     previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-details\">\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n    <div class=\"dz-size\" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class=\"progress progress-small progress-striped active\"><div class=\"progress-bar progress-bar-success\" data-dz-uploadprogress></div></div>\n  <div class=\"dz-success-mark\"><span></span></div>\n  <div class=\"dz-error-mark\"><span></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>"
     });
     } catch (e) {
     alert('Dropzone.js does not support older browsers!');
     }
     myDropzone.on("success", function (file, response) {
     file.name = response;
     $('.files').append('<input type="hidden" name="imagen[]" class="' + $.trim(response) + '" id="' + $.trim(response) + '" value="' + $.trim(response) + '">')
     });
     myDropzone.on("removedfile", function (file) {
     // console.log($('.files').find('#' + $.trim(file.name)));
     });*/
    $('#fecha').datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true,
    });

    $("#categorias").select2();
    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fecha: {
                validators: {
                    notEmpty: {
                        message: 'Campo requerido'
                    }
                }
            },
            obra: {
                validators: {
                    notEmpty: {
                        message: 'Campo requerido'
                    }
                }
            },
            categorias: {
                validators: {
                    notEmpty: {
                        message: 'Campo requerido'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $('button[type=submit]').text('Guardando...').prop('disabled', true);
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                // alert("guardado");
                // $("#modal_form").modal("hide");
                // $('.data-table').bootstrapTable('refresh');
                window.location.href = "main.php?page=galerias&title=Fotos";
            }
        }, 'json');
    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

    $('#subir').click(function () {
        var form_data = new FormData();
        form_data.append('imagen', $('#imagen').prop('files')[0]);
        swal({
            title: 'Procesando imagen...',
            text: '<span class="fa fa-refresh fa-spin fa-3x"></span>',
            html: true,
            type: 'warning',
            allowOutsideClick: false,
            showConfirmButton: false,
        })
        $.ajax({
            async: true,
            url: 'upload.php',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            beforeSend: function (response) {
            },
            success: function (response) {
                swal.close()
                $('.links').append('<div class="file-container"><div class="col-md-2 text-center"><img class="img-thumbnail ' + (response.split('.')[0]).trim() + '" style="height: 110px;" src="images/blogmanagement/obras/thumb/' + response + '"><input type="hidden" name="imagen[]" value="' + response + '"><div class="clearfix"></div><a class="' + response + '" href="images/blogmanagement/obras/big/' + response + '" target="_blank">' + response + ' </a><button class="btn btn-flat btn-primary rotate-image" data-imagen="' + response + '" data-grados="90" type="button"><span class="fa fa-undo"></span></button> <button class="btn btn-flat btn-primary rotate-image" data-imagen="' + response + '" data-grados="-90" type="button"><span class="fa fa-repeat"></span></button> <button type="button" class="btn btn-flat btn-danger btn-block delete-image"> <span class="fa fa-times"></span></button><br></div></div>')
                $('#imagen').val('')
                $('#file-trigger').text('Seleccionar archivo')
            }
        });
    })
    $('#file-trigger').click(function () {
        $('#imagen').click()
    })
    $('#imagen').change(function () {
        $('#file-trigger').text($('input[type=file]').val().replace(/C:\\fakepath\\/i, ''))
    })
    $('.links')
        .on('click', '.delete-image', function () {
            $(this).parent().remove()
        })
        .on('click', '.rotate-image', function () {
            var imagen = $(this).data('imagen')
            swal({
                title: 'Procesando imagen...',
                text: '<span class="fa fa-refresh fa-spin fa-3x"></span>',
                html: true,
                type: 'warning',
                allowOutsideClick: false,
                showConfirmButton: false,
            })
            $.ajax({
                async: true,
                url: 'rotate.php',
                type: 'POST',
                data: {
                    imagen: $(this).data('imagen'),
                    grados: $(this).data('grados'),
                },
                beforeSend: function (response) {
                },
                success: function (response) {
                    swal.close()
                    $('.links img.' + response.split('.')[0]).attr('src', 'images/blogmanagement/obras/big/' + imagen + "?v=" + (new Date()).getTime())
                }
            });
        })
});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew() {
    $('.modal-title').text('Nueva categoria');
    $(".clearval").val('');
    $('#frm #action').val($('#action').val());
    url = 'new.php';
    return false;
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Eliminar usuario');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = {
        action: 'user',
        Id: id,
    }
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doChangePass(id, name) {
    $(".modal-title-pass").text('Change Password ' + name);
    $('#modal_pass').modal('show');
    $('.clearval').val('')
    $('#frmPass input[name=Id]').val(id);
    url = 'update.php';
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}

function autocompleteUserLevels() {
    $.post('autocompletar.php', {action: 'user_levels'}, function (result) {
        if (result) {
            $('#level').html(result)
        }
    }, 'json');
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Eliminar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar categoria ' + row.category);
        url = 'update.php';
        $.each(row, function (key, value) {
            console.log(key + ' - ' + value);
            $('#frm #' + key).val(value);
        });
        $('#modal_form').modal('show');
    },

    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar la categoria ' + row.category);
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.Id);
        console.log(value, row, index);
    }
};