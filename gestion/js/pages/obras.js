var q = '';
var campo = '';
var srt = '';
var order = '';
var page = '1';
var url;

$(document).ready(function () {

    autocompleteUserLevels();

    $('.fecha_input').datepicker({
        format: 'mm/dd/yyyy',
        language: 'en'
    });

    $(".email").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    });
    $('.email').change(function () {
        $('button[type=submit]').prop('disabled', false)
    })

    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nombre: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },
            porcentaje: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },
            lugar: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },
            email: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },
            mostrar: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $('button[type=submit]').text('Guardando...').prop('disabled', true);
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                $("#modal_form").modal("hide");
                $('.data-table').bootstrapTable('refresh');
                $('button[type=submit]').text('Guardar').prop('disabled', false);
            }
        }, 'json');
    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

    $('.slider').slider();

    /* ION SLIDER */
    $("#range_1").ionRangeSlider({
        min: 0,
        max: 5000,
        from: 1000,
        to: 4000,
        type: 'double',
        step: 1,
        prefix: "$",
        prettify: false,
        hasGrid: true
    });
    $("#range_2").ionRangeSlider({});

    $(".range_5").ionRangeSlider({
        min: 0,
        max: 100,
        type: 'single',
        step: 5,
        postfix: " %",
        prettify: false,
        hasGrid: true,
        onChange: function (obj) {
            $('button[type=submit]').prop('disabled', false);
            var t = "";
            for (var prop in obj) {
                t += prop + ": " + obj[prop] + "\r\n";
            }
        },
    });
    $("#range_6").ionRangeSlider({
        min: -50,
        max: 50,
        from: 0,
        type: 'single',
        step: 1,
        postfix: "°",
        prettify: false,
        hasGrid: true
    });

    $("#range_4").ionRangeSlider({
        type: "single",
        step: 100,
        postfix: " light years",
        from: 55000,
        hideMinMax: true,
        hideFromTo: false,
    });
    $("#range_3").ionRangeSlider({
        type: "double",
        postfix: " miles",
        step: 10000,
        from: 25000000,
        to: 35000000,
        onChange: function (obj) {
            var t = "";
            for (var prop in obj) {
                t += prop + ": " + obj[prop] + "\r\n";
            }
            $("#result").html(t);
        },
        onLoad: function (obj) {
            //
        }
    });

});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew() {
    $('.modal-title').text('Nueva obra');
    $(".clearval").val('');
    $('#frm #action').val($('#action').val());
    url = 'new.php';
    return false;
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Eliminar usuario');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = {
        action: 'user',
        Id: id,
    }
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doChangePass(id, name) {
    $(".modal-title-pass").text('Change Password ' + name);
    $('#modal_pass').modal('show');
    $('.clearval').val('')
    $('#frmPass input[name=Id]').val(id);
    url = 'update.php';
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}

function autocompleteUserLevels() {
    $.post('autocompletar.php', {action: 'user_levels'}, function (result) {
        if (result) {
            $('#level').html(result)
        }
    }, 'json');
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Elimiar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar obra ' + row.category);
        url = 'update.php';
        $.each(row, function (key, value) {
            console.log(key + ' - ' + value);
            if (key == 'email') {
                var emails = value.split(',');
                $('#email').empty()
                $.each(emails, function (index, value) {
                    $('#email').append('<option value="' + value + '" selected>' + value + '</option>')
                })
            }
            else
                $('#frm #' + key).val(value);
        });
        $('#modal_form').modal('show');
    },

    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar la categoria ' + row.category);
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.Id);
        console.log(value, row, index);
    }
};