var q = '';
var order = '';
var page = '1';
var url;

$(document).ready(function () {


    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew() {
    location.href = 'main.php?page=property_create';
}

function doEdit(id) {
    window.location.href = 'main.php?page=property_create&Id=' + id;
}

function doAlertRemove(id) {
    $(".modal-title").text('Eliminar post');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
}

function doRemove() {

    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: $('#frmDelete').serialize(),
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);
        }
    });
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '<li><a class="show"  href="javascript:void(0)"><i class="fa fa-eye"></i> Ver</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Elimiar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {
    'click .edit': function (e, value, row, index) {
        doEdit(row.Id);
    },
    'click .show': function (e, value, row, index) {
        window.open('http://orsettipropiedades.com.ar/properties-detail.php?property=' + row.Id, '_blank');
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar la publicacion');
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.Id);
    }
};