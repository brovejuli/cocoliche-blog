var url = 'new.php';
var video_id;
$(document).ready(function () {
    $('#fecha').datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true,
    });

    $("#categorias").select2();
    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fecha: {
                validators: {
                    notEmpty: {
                        message: 'Campo requerido'
                    }
                }
            },
            obra: {
                validators: {
                    notEmpty: {
                        message: 'Campo requerido'
                    }
                }
            },
            categorias: {
                validators: {
                    notEmpty: {
                        message: 'Campo requerido'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $('button[type=submit]').text('Guardando...').prop('disabled', true);
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                // alert("guardado");
                // $("#modal_form").modal("hide");
                // $('.data-table').bootstrapTable('refresh');
                window.location.href = "main.php?page=videos&title=Videos";
            }
        }, 'json');
    });
})
/*
 Copyright 2015 Google Inc. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

var signinCallback = function (result) {
    if (result.access_token) {
        var uploadVideo = new UploadVideo();
        uploadVideo.ready(result.access_token);
    }
};

var STATUS_POLLING_INTERVAL_MILLIS = 60 * 1000; // One minute.


/**
 * YouTube video uploader class
 *
 * @constructor
 */
var UploadVideo = function () {
    /**
     * The array of tags for the new YouTube video.
     *
     * @attribute tags
     * @type Array.<string>
     * @default ['google-cors-upload']
     */
    this.tags = ['youtube-cors-upload'];

    /**
     * The numeric YouTube
     * [category id](https://developers.google.com/apis-explorer/#p/youtube/v3/youtube.videoCategories.list?part=snippet®ionCode=us).
     *
     * @attribute categoryId
     * @type number
     * @default 22
     */
    this.categoryId = 22;

    /**
     * The id of the new video.
     *
     * @attribute videoId
     * @type string
     * @default ''
     */
    this.videoId = '';

    this.uploadStartTime = 0;
};


UploadVideo.prototype.ready = function (accessToken) {
    this.accessToken = accessToken;
    this.gapi = gapi;
    this.authenticated = true;
    this.gapi.client.request({
        path: '/youtube/v3/channels',
        params: {
            part: 'snippet',
            mine: true
        },
        callback: function (response) {
            if (response.error) {
                console.log(response.error.message);
            } else {
                $('#channel-name').text(response.items[0].snippet.title);
                $('#channel-thumbnail').attr('src', response.items[0].snippet.thumbnails.default.url);

                $('.pre-sign-in').hide();
                $('.post-sign-in').show().css('visibility', 'visible');
            }
        }.bind(this)
    });
    $('#button').on("click", this.handleUploadClicked.bind(this));
    $('#button').click(function () {
        $('.btn-save').prop('disabled', true);
        $('#post-upload-status').html('');
    });
};

/**
 * Uploads a video file to YouTube.
 *
 * @method uploadFile
 * @param {object} file File object corresponding to the video to upload.
 */
UploadVideo.prototype.uploadFile = function (file) {
    var metadata = {
        snippet: {
            title: $('#title').val(),
            description: $('#description').text(),
            tags: this.tags,
            categoryId: this.categoryId
        },
        status: {
            privacyStatus: $('#privacy-status option:selected').val()
        }
    };
    var uploader = new MediaUploader({
        baseUrl: 'https://www.googleapis.com/upload/youtube/v3/videos',
        file: file,
        token: this.accessToken,
        metadata: metadata,
        params: {
            part: Object.keys(metadata).join(',')
        },
        onError: function (data) {
            var message = data;
            // Assuming the error is raised by the YouTube API, data will be
            // a JSON string with error.message set. That may not be the
            // only time onError will be raised, though.
            try {
                var errorResponse = JSON.parse(data);
                message = errorResponse.error.message;
            } finally {
                alert(message);
            }
        }.bind(this),
        onProgress: function (data) {
            var currentTime = Date.now();
            var bytesUploaded = data.loaded;
            var totalBytes = data.total;
            // The times are in millis, so we need to divide by 1000 to get seconds.
            var bytesPerSecond = bytesUploaded / ((currentTime - this.uploadStartTime) / 1000);
            var estimatedSecondsRemaining = (totalBytes - bytesUploaded) / bytesPerSecond;
            var percentageComplete = (bytesUploaded * 100) / totalBytes;

            $('#upload-progress').attr({
                value: bytesUploaded,
                max: totalBytes
            });
            $('.upload-progress')
                .attr({
                    'aria-valuenow': bytesUploaded,
                    'aria-valuemax': totalBytes
                })
                .css({
                    width: percentageComplete + '%'
                });
            $('#percent-transferred').text(Math.ceil(percentageComplete));
            // $('#bytes-transferred').text(bytesUploaded);
            // $('#total-bytes').text(totalBytes);

            $('.during-upload').show();
        }.bind(this),
        onComplete: function (data) {
            $('.box-loading').removeClass('hidden');
            var uploadResponse = JSON.parse(data);
            this.videoId = uploadResponse.id;
            $('#video-id').text(this.videoId);
            video_id = this.videoId;
            $('.post-upload').show();
            this.pollForVideoStatus();

        }.bind(this)
    });
    // This won't correspond to the *exact* start of the upload, but it should be close enough.
    this.uploadStartTime = Date.now();
    uploader.upload();
};

UploadVideo.prototype.handleUploadClicked = function () {
    $('#button').attr('disabled', true);
    this.uploadFile($('#file').get(0).files[0]);
};

UploadVideo.prototype.pollForVideoStatus = function () {
    this.gapi.client.request({
        path: '/youtube/v3/videos',
        params: {
            part: 'status,player',
            id: this.videoId
        },
        callback: function (response) {
            if (response.error) {
                // The status polling failed.
                console.log(response.error.message);
                setTimeout(this.pollForVideoStatus.bind(this), STATUS_POLLING_INTERVAL_MILLIS);
            } else {
                var uploadStatus = response.items[0].status.uploadStatus;
                switch (uploadStatus) {
                    // This is a non-final status, so we need to poll again.
                    case 'uploaded':
                        // $('#post-upload-status').append('<li>Upload status: ' + uploadStatus + '</li>');
                        setTimeout(this.pollForVideoStatus.bind(this), STATUS_POLLING_INTERVAL_MILLIS);
                        break;
                    // The video was successfully transcoded and is available.
                    case 'processed':
                        $('.box-loading').addClass('hidden');
                        // $('#player').html(response.items[0].player.embedHtml);
                        // $('#post-upload-status').html('<li>Final status.</li>');
                        $('.videos').append('<input type="hidden" name="videos[]" value="' + video_id + '">')
                        $('#button').attr('disabled', false);
                        $('.videos-links').append('<a href="https://www.youtube.com/watch?v=' + video_id + '" target="_blank">https://www.youtube.com/watch?v=' + video_id + '</a>')
                        $('#title').val('');
                        $('#description').val('');
                        $('.upload-progress')
                            .attr({
                                'aria-valuenow': 0,
                                'aria-valuemax': 0
                            })
                            .css({
                                width: '0%'
                            });
                        $('.btn-save').prop('disabled', false);
                        $('#post-upload-status').html('');
                        break;
                    // All other statuses indicate a permanent transcoding failure.
                    default:
                        $('#post-upload-status').append('<p>Ocurrio un error, por favor intentelo nuevamente.</p><p>El error puede deberse a:<br><li>Violaci&oacute;n de derechos de autor.</li><li>El video ya se encuentra subido al canal.</li></p>');
                        $('#button').attr('disabled', false);
                        $('.box-loading').addClass('hidden');
                        $('.upload-progress')
                            .attr({
                                'aria-valuenow': 0,
                                'aria-valuemax': 0
                            })
                            .css({
                                width: '0%'
                            });
                        $('.btn-save').prop('disabled', false);
                        break;
                }
            }
        }.bind(this)
    });
};