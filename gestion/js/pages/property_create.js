var q = '';
var campo = '';
var srt = '';
var order = '';
var page = '1';
var urlfile = '';
const IMG_SRC = "../images/blogmanagement/thumbs/";

$(document).ready(function () {

    autocompleteProvinces();

    $('#province').change(function () {
        autocompleteCities($(this).val());
    })

    if ($('#Id').val()) {

        $('.box-header').find('h1 span').text('Editar publicación');
        $('.savePostButton').attr('onclick', "doUpdate()");
        autocompletePostData($('#Id').val());

    }
    else {
        appendSliderImages();
        $('#frm').removeClass('hidden');
        $('.loading-content').addClass('hidden');
        tinymceInit();
    }
    /*Sortable for drag & drop slider images*/
    $("#sortable").sortable();
    $("#sortable").disableSelection();

    $('#sortable').on('click', '.delete-slider-image', function (e) {
        e.preventDefault();
        $(this).parent().parent().attr('img-src', '');
        $(this).prev('a').find('div img').remove().html('<i class="fa fa-plus fa-4x"></i>');
        $(this).prev('a').find('div').html('<i class="fa fa-plus fa-4x"></i>');
        $(this).addClass('hidden');

    });

    $('.delete-promo-image').click(function (e) {
        e.preventDefault();
        $(this).parent().parent().attr('img-src', '');
        $(this).prev('a').find('div img').remove().html('<i class="fa fa-plus fa-4x"></i>');
        $(this).prev('a').find('div').html('<i class="fa fa-plus fa-4x"></i>');
        $(this).addClass('hidden');
        $('input[name=promotion_image]').val('');
    });

    //Bootstrap Datepicker
    $("#validity_from, #validity_to").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true,
    }).on('changeDate', function (e) {
        // Revalidate it
        $('#frm').bootstrapValidator('revalidateField', 'validity_from');
        $('#frm').bootstrapValidator('revalidateField', 'validity_to');
    });

    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                message: 'El titulo no es valido',
                validators: {
                    notEmpty: {
                        message: 'El titulo no puede estar vacio!.'
                    },
                    stringLength: {
                        min: 3,
                        max: 300,
                        message: 'El nombre de la categoria debe tener entre 5 y 300 caracteres!'
                    },
                }
            },
            date: {
                // The hidden input will not be ignored
                excluded: false,
                validators: {
                    notEmpty: {
                        message: 'Por favor ingresar una fecha valida (dd/mm/aaaa).'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'La fecha ingresada no es valida!'
                    },
                }
            },
            important: {
                message: 'El titulo no es valido',
                validators: {
                    notEmpty: {
                        message: 'El titulo no puede estar vacio!.'
                    },
                    stringLength: {
                        min: 3,
                        max: 300,
                        message: 'El nombre de la categoria debe tener entre 5 y 300 caracteres!'
                    },
                }
            },
        }
    }).on('success.form.bv', function (e) {
        if (!$('.files').html()) {
            alert("Seleccionar al menos una imagen de portada.");
            $('.files').focus();
            $('#frm').data('bootstrapValidator').disableSubmitButtons(false);
            return false;
        }

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                $("#create-trip-success").modal({backdrop: 'static', keyboard: false});
            }
        }, 'json');


    });

});

function doNew(draft) {
    $('#draft').val(draft);
    tinymce.triggerSave();//Esto sirve para que se actualize en contenido del textarea
    setFileList();
    url = 'new.php';
    return false;
}

function doUpdate(draft) {
    $('#draft').val(draft);
    tinymce.triggerSave();//Esto sirve para que se actualize en contenido del textarea
    setFileList();
    url = 'update.php';
    return false;
}

function justNumbers(e) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if ((keynum == 8) || (keynum == 46))
        return true;

    return /\d/.test(String.fromCharCode(keynum));
}

$('.iframe-btn').fancybox({
    'width': 900,
    'height': 600,
    'type': 'iframe',
    'autoScale': false
});

function responsive_filemanager_callback(field_id) {
    var url = jQuery('#' + field_id).val();
    var imgName = url.split('/');

    $('#' + field_id).html('<img id="fileManagerImgSrc" src="' + url + '" class="img-thumbnail"/>');
    $('#' + field_id).parent().parent().parent().attr('img-src', url);
    $('#' + field_id).parent().parent().parent().find('button').removeClass('hidden');

}

function setFileList() {
    var sortedImgs = $("#sortable").sortable('toArray', {attribute: 'img-src'});
    //console.log(sortedImgs);
    var imgsToAppend = '';
    for (var i = 1; i < sortedImgs.length; i++) {
        if (sortedImgs[i])
            imgsToAppend += '<input type="hidden" name="files[]" value="' + sortedImgs[i] + '"/>'
    }
    $('.files').html(imgsToAppend);
}

function autocompletePostData(id) {
    var images = [];
    $.post('autocompletar.php', {action: 'property', Id: id}, function (result) {
        $('#frm').removeClass('hidden');
        $('.loading-content').addClass('hidden');
        $.each(result.post, function (field, value) {
            if (field == 'city')
                autocompleteCities($('#province').val());
            $('#' + field).val(value);
        });
        tinymceInit();
        $.each(result.images, function (index) {
            $.each(this, function (field, value) {
                images.push([(index + 1), value]);
            });
        });
        appendSliderImages(images);

    }, 'json');
}


function appendSliderImages(images) {
    for (var i = 1; i <= 6; i++) {
        if (undefined !== images && images.length >= i) {
            var container_top = '<div class="text-center col-xs-6 col-md-2" img-src="' + images[i - 1][1] + '">';
            var thumbnail_top = '<div class="sortable-image-wrapper  thumbnail" img-src="' + images[i - 1][1] + '">';
            var image = '<div id="inserted_image_' + i + '" class="slider-image thumbnail"><img src="' + images[i - 1][1] + '"></div>';
            var button = '<button class="delete-slider-image btn btn-custom btn-block btn-flat">BORRAR</button>';
        }
        else {
            var container_top = '<div class="text-center col-xs-6 col-md-2">';
            var thumbnail_top = '<div class="sortable-image-wrapper  thumbnail" img-src="">';
            var image = '<div id="inserted_image_' + i + '" class="slider-image"><i class="fa fa-plus fa-4x"></i></div>';
            var button = '<button class="delete-slider-image btn btn-custom btn-block btn-flat hidden">BORRAR</button>';
        }
        var container_bottom = '</div>';
        var thumbnail_bottom = '</div>';
        var iframe_btn_top = '<a href="vendor/bower_components/tinymce/plugins/responsivefilemanager/filemanager/dialog.php?type=1&field_id=inserted_image_' + i + '" class="btn iframe-btn" type="button">';
        var iframe_btn_bottom = '</a>';
        $('#sortable').append(container_top + thumbnail_top + iframe_btn_top + image + iframe_btn_bottom + button + thumbnail_bottom + container_bottom);
    }
}
function tinymceInit() {

    tinymce.init({
        selector: ".richtext-full", theme: "modern", width: '100%', height: 300,
        menubar: false,
        plugins: ["advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"],
        toolbar1: "bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        toolbar2: "| responsivefilemanager image media | forecolor backcolor  | print preview ",
        image_advtab: true,
        relative_urls: false,
        external_filemanager_path: "vendor/bower_components/tinymce/plugins/responsivefilemanager/filemanager/",
        filemanager_title: "Gestor de archivos - Todo Organizado",
        filemanager_access_key: "566827e8-f952-4021-b1b3-9cb28c298a28",
        external_plugins: {"filemanager": "plugins/responsivefilemanager/plugin.min.js"},
        language: "es_AR",
    });
    tinymce.init({
        selector: ".richtext-basic", theme: "modern", width: '100%', height: 150,
        menubar: false,
        plugins: [
            "searchreplace wordcount  visualchars insertdatetime media nonbreaking",
        ],
        toolbar1: "bold italic underline | alignleft aligncenter alignright alignjustify | styleselect",
        language: "es_AR",
        setup: function (ed) {
            ed.on('init', function (args) {
                console.log(args.target.id);
            });
        }
    });
}

function autocompleteProvinces() {
    $.ajax({
        async: false,
        url: 'autocompletar.php',
        type: 'POST',
        data: {
            action: 'provinces',
        },
        dataType: 'json',
        beforeSend: function (response) {
        },
        success: function (response) {
            $.each(response, function (i, item) {
                $('#province').append($('<option>', {
                    value: item.id,
                    text: item.province
                }));
            });
            autocompleteCities($('#province').val());
        }
    });
}

function autocompleteCities(province_id) {
    $.ajax({
        async: false,
        url: 'autocompletar.php',
        type: 'POST',
        data: {
            action: 'cities',
            province: province_id
        },
        dataType: 'json',
        beforeSend: function (response) {
        },
        success: function (response) {
            $('#city').empty();
            $.each(response, function (i, item) {
                $('#city').append($('<option>', {
                    value: item.id,
                    text: item.city
                }));
            });
        }
    });
}