$(document).ready(function(){

	$('.just-numbers').keypress(function (e) {
		var keynum = window.event ? window.event.keyCode : e.which;
		if ((keynum == 8) || (keynum == 46))
			return true;

		return /\d/.test(String.fromCharCode(keynum));
	})


});
function swal_procesando() {
	swal({
		title: 'Procesando solicitud...',
		text: '<span class="fa fa-refresh fa-spin fa-3x"></span>',
		html: true,
		type: 'warning',
		allowOutsideClick: false,
		showConfirmButton: false,
	})
}