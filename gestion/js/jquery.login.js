// JavaScript Document

$(document).ready(function() {	
	$("#username").focus();

	$('.frmError').hide(); //error content
	$('.frmError').click(function(){$(this).fadeOut('fast'); $(this).prev().focus()});
	$("#login").validate({
		rules: {
			username: {
				required: true,
			},
			password: {
				required: true,
			}
		},
		messages: {
        	username: "<i class='fa fa-exclamation'></i> Ingrese un usuario",
			password: "<i class='fa fa-exclamation'></i> Ingrese su contrase&ntilde;a",
		},
		invalidHandler: function(event, validator) {
			$(this).find('.error').next().fadeIn().delay(1000).fadeOut();

		},
		errorPlacement: function (error, element) {
			element.next().fadeIn().delay(1000).fadeOut();
			element.next().html(error);
		},
		submitHandler: function(form){
			login();
		}
	});

});

function login(){

		var str = $("#login").serialize();
		$.ajax({
			type: "POST",
			url: "login.php",
			data: str,
			dataType: 'json',
			success: function(data){
				if (data.error)
				{
					$('#msgError').html(data.error);
					$("#password").focus();
				} else if (data.success) {
					window.location.href= "main.php?page=galerias&title=Fotos";
				}
			}
		 });
		return false;
		
}