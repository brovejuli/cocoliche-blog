-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2017 at 03:18 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `power_cocoliche`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `Id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `copete` text,
  `cuerpo` text,
  `usuario` int(11) DEFAULT NULL,
  `borrar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`Id`, `fecha`, `titulo`, `copete`, `cuerpo`, `usuario`, `borrar`) VALUES
(4, '2017-05-29', 'Entrada de pruebá 1', '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><b>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</b></span><br></p>', '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</span></p><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</span></p><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><br></p>', 0, 0),
(5, '2017-05-29', 'Entrada de prueña 2 "Test"', '<blockquote><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><u>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</u></span></blockquote>', '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</span></p><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><br></p>', 0, 0),
(6, '0000-00-00', 'Entrada de prueba 3 - Editada', '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><strike>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</strike>."</span><br></p>', '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</span><br></p>', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `blog_imagenes`
--

CREATE TABLE `blog_imagenes` (
  `Id` int(11) NOT NULL,
  `blog` int(11) DEFAULT NULL,
  `archivo` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_imagenes`
--

INSERT INTO `blog_imagenes` (`Id`, `blog`, `archivo`) VALUES
(8, 4, '592c659df11b8.jpg'),
(9, 5, '592c65bf689a0.jpg'),
(11, 6, '592c65eb930aa.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `Id` int(11) NOT NULL,
  `categoria` varchar(255) DEFAULT NULL,
  `mostrar` tinyint(2) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`Id`, `categoria`, `mostrar`) VALUES
(1, 'Cat 1', 1),
(2, 'Cat 2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `Id` int(11) NOT NULL,
  `variable` char(255) NOT NULL,
  `value` char(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`Id`, `variable`, `value`) VALUES
(1, 'MailHost', 'mail.cocoliche.com'),
(2, 'SMTPAuth', 'true'),
(3, 'Mail_Port', '587'),
(4, 'Mail_Username', ''),
(5, 'Mail_Password', 'mailPassword'),
(6, 'Mail_From', 'hola@cocoliche.com'),
(7, 'Mail_From_Name', 'Cocoliche'),
(8, 'Title', ''),
(11, 'Empresa_Nombre', 'Cocoliche'),
(13, 'Empresa_www', 'www.cocoliche.com'),
(14, 'Empresa_Telefono', '0221 4961524'),
(15, 'Youtube_Video_Url', ''),
(16, 'Url_Facebook', 'https://www.facebook.com/CocolicheRopaconotraOportunidad/'),
(17, 'Url_Twitter', ''),
(18, 'Url_Pinterest', ''),
(19, 'Url_Instagram', 'https://www.instagram.com/explore/locations/1022363353/'),
(20, 'Url_Linkedin', ''),
(21, 'Metatags_Description', ''),
(22, 'Metatags_Keywords', ''),
(23, 'Metatags_Author', ''),
(24, 'Empresa_Direccion', 'Diagonal 77 #992 LA PLATA'),
(25, 'Empresa_Localidad', 'La Plata, Buenos Aires'),
(26, 'Empresa_Movil', ''),
(27, 'Url_Google', ''),
(28, 'Pagination_Limit', '15');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `Id` int(11) NOT NULL,
  `level` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`Id`, `level`) VALUES
(0, 'Admin'),
(2, 'Blog master'),
(3, 'Cursos master');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `Id` char(10) NOT NULL,
  `Nombre` char(30) NOT NULL,
  `Grupo` char(50) NOT NULL,
  `Titulo` char(50) NOT NULL,
  `Link` char(30) NOT NULL,
  `iconCls` char(30) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`Id`, `Nombre`, `Grupo`, `Titulo`, `Link`, `iconCls`, `level`) VALUES
('0', 'Blog', 'Blog', 'Blog', 'listar_blog', 'fa-rss', 2),
('3', 'Categorias', 'Categorias', 'Categorias', 'categorias', 'fa-list', 2),
('8', 'Usuarios', 'Usuarios', 'Usuarios', 'users', 'fa-user', 1),
('9', 'Configuracion', 'Configuracion', 'Configuracion', 'configuration', 'fa-cogs', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` bigint(32) NOT NULL,
  `name` varchar(100) NOT NULL,
  `user` char(40) NOT NULL,
  `pass` text NOT NULL,
  `level` int(1) NOT NULL,
  `borrar` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `name`, `user`, `pass`, `level`, `borrar`) VALUES
(9, 'Federico', 'fede', '7832756692e6f1a77da5b29e458a7422', 0, 0),
(21, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `blog_imagenes`
--
ALTER TABLE `blog_imagenes`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `blog_imagenes_blog_Id_fk` (`blog`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `variable` (`variable`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `user` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `blog_imagenes`
--
ALTER TABLE `blog_imagenes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `Id` bigint(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_imagenes`
--
ALTER TABLE `blog_imagenes`
  ADD CONSTRAINT `blog_imagenes_blog_Id_fk` FOREIGN KEY (`blog`) REFERENCES `blog` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
