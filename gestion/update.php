<?php
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . '/validacion.php');
require_once(__ROOTWEB__ . '/class/Configuration.class.php');
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
require_once(__ROOTWEB__ . '/class/Main.class.php');


$databaseManager = new DatabaseManager();

if (!empty($_REQUEST["action"])) {

    $logic = ["No" => 0, "Si" => 1];

    switch ($_POST["action"]) {

        case "user":

            if ($databaseManager->update(TABLE_USERS, $_POST)) {
                echo json_encode(array('status' => true, 'msg' => 'Usuario actualizado correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar el usuario. Por favor intentalo de nuevo!'));

            break;

        case "users_pass":
            require(__ROOTWEB__ . "/class/users.class.php");

            $obj = new Users();
            $id = $_REQUEST["Id"];
            $pass = md5($_REQUEST["pass"]);

            if ($obj->actualizar_pass($pass, $id) == true) {
                $obj->register_logs(array(utf8_decode($id . ': UPDATE CHANGE PASSWORD '), $USER));
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'An error occurred, check whether the record exists try again.'));

            break;


        case "configuration":

            if ($databaseManager->update(TABLE_CONFIGURATION, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Categoria actualizada correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar la categoria. Por favor intentalo de nuevo!'));

            break;

        case "categoria":

            if ($databaseManager->update(TABLE_CATEGORIAS, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Categoria actualizada correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar la categoria. Por favor intentalo de nuevo!'));

            break;

        case 'blog':
            $_REQUEST['usuario'] = $USER;
            if ($databaseManager->update(TABLE_BLOG, $_REQUEST)) {
                if (isset($_REQUEST['archivo'])) {
                    $databaseManager->remove(TABLE_BLOG_IMAGENES, $_REQUEST['Id'], 'blog');
                    foreach ($_REQUEST['archivo'] as $imagen) {
                        $datos = [
                            'archivo' => $imagen,
                            'blog'    => $_REQUEST['Id'],
                        ];
                        $databaseManager->save(TABLE_BLOG_IMAGENES, $datos);
                    }
                }
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

    }
}

?>