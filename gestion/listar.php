<?php
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . '/validacion.php');
require_once(__ROOTWEB__ . '/class/Configuration.class.php');
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
require_once(__ROOTWEB__ . '/class/Main.class.php');

$logic = array(0 => "NO", 1 => "SI");
$databaseManager = new DatabaseManager();

if (!empty($_REQUEST["action"])) {

    switch ($_REQUEST["action"]) {

        case "configuration":

            $query = $databaseManager->select(TABLE_CONFIGURATION, 'variable', 'ASC');
            echo json_encode(Main::utf8_converter($query));

            break;

        case "users":

            require_once(__ROOTWEB__ . '/class/Users.class.php');

            $users = new Users();


            $search = !empty($_POST['q']) ? $_POST['q'] : '';
            $field = !empty($_POST['campo']) ? $_POST['campo'] : '';
            $source = !empty($_POST['source']) ? $_POST['source'] : '';

            $table = TABLE_USERS;
            $query = $users->select($rows, $srt, $order, $offset, $field, $search);

            echo json_encode(Main::utf8_converter($query));

            break;

        case "blogs":

            include_once 'class/Blog.class.php';

            $obj = new Blog();

            $query = $obj->select();

            echo json_encode(Main::query_converter($query));

            break;

        case "blog":


            $query['blog'] = Main::query_converter($databaseManager->select(TABLE_BLOG, 'titulo', 'DESC', 1, $_REQUEST['Id'])[0]);
            $query['imagenes'] = $databaseManager->select(TABLE_BLOG_IMAGENES, 'Id', 'DESC', 0, $_REQUEST['Id'], 'blog')[0];

            echo json_encode($query);

            break;

        case "categorias":

            $query = $databaseManager->select(TABLE_CATEGORIAS, 'categoria', 'DESC');

            echo json_encode(Main::query_converter($query));

            break;

    }


}


?>