<?php
//  Autentificacion
session_start();
//error_reporting(0);
// if session is not set redirect the user
if(empty($_SESSION['UserSystem']) || isset($_GET['logout']))
{
	session_destroy();
	header ("location:index.php");		
} else {
	
	$IDUSER = $_SESSION['IDUser'];
	$USER = $_SESSION['UserSystem'];
	$NAME = $_SESSION['Name'];
	$PROVIDER = $_SESSION['IDProvider'];
	$ACCESS = $_SESSION['ACCESS'];
	
	setlocale(LC_MONETARY, 'es_AR');
}
?>