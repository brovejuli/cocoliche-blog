<?php
require_once("class/Autocomplete.class.php");
require_once("class/Main.class.php");
require_once("includes/database_tables.php");
$obj = new Autocomplete();
if (!empty($_REQUEST["action"])) {

    $search_val = strtolower($_REQUEST["term"]);

    switch ($_REQUEST["action"]) {

        case 'provinces':
            $provinces = $obj->provinces();
            echo json_encode(Main::utf8_converter($provinces) );
            break;

        case 'cities':
            $cities = $obj->cities($_REQUEST['province']);
            echo json_encode(Main::utf8_converter($cities));
            break;

        case 'property':
            $property['post'] = Main::utf8_converter($obj->property($_REQUEST['Id'])) ;
            $property['images'] = $obj->property_images($_REQUEST['Id']);
            echo json_encode($property);
            break;

        default:
            echo "Ocurrio un error, intentelo nuevamente";
    }
}
?>