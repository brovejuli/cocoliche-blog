<?php
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . '/validacion.php');
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
require_once(__ROOTWEB__ . '/class/Main.class.php');

$databaseManager = new DatabaseManager();

if (!empty($_POST["action"])) {
    switch ($_POST["action"]) {

        case "user":

            if ($databaseManager->remove(TABLE_USERS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'An error occurred, check whether the record exists try again.'));

            break;

        case "categoria":

            if ($databaseManager->remove(TABLE_CATEGORIAS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "blog":

            if ($databaseManager->remove(TABLE_BLOG, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "galeria":

            if ($databaseManager->remove(TABLE_FOTOS_GALERIA, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "galeria_videos":

            if ($databaseManager->disable(TABLE_VIDEOS, $_REQUEST['Id']) && $databaseManager->disable(TABLE_VIDEOS_GALERIA, $_REQUEST['Id'], 'id_video')) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "property":

            if ($databaseManager->remove(TABLE_PROPERTIES, $_REQUEST['Id'])) {
                $databaseManager->remove(TABLE_PROPERTIES_IMAGES, $_REQUEST['Id'], 'property');
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'An error occurred, check whether the record exists try again.'));

            break;

    }
}

?>