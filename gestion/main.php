<?php
include_once("validacion.php");
include_once("includes/configure.php");
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . "/class/Main.class.php");

$obj = new Main();

include(DIR_WS_COMMON . "doctype-and-head.php");
include(DIR_WS_COMMON . "nav-and-title.php");

if (!empty($_GET['page'])) {

    $folder = "pages/";
    $FILE = $folder . $_GET['page'] . ".php";
    $blank = $folder . "blank.php";
    $exceptions = [
        'welcome',
        'nuevo_blog',
    ];
    if (file_exists($FILE) && ($obj->level_file($_GET["page"], $ACCESS) || (in_array($_GET["page"], $exceptions))))
        include($FILE);
    else
        include($blank);

}


include("common/footer.php");
?>


<!--==============================FOOTER=================================-->
