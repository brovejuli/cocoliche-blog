<?php
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . '/validacion.php');
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
require_once(__ROOTWEB__ . '/class/Main.class.php');
require_once(__ROOTWEB__ . '/class/Email.class.php');

$databaseManager = new DatabaseManager();

if (!empty($_POST["action"])) {

    $logic = ["No" => 0, "Si" => 1];

    switch ($_POST["action"]) {

        case "user":

            if ($last_inserted = $databaseManager->save(TABLE_USERS, $_POST)) {
                echo json_encode(array('status' => true, 'msg' => 'Usuario insertado correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar insertar el usuario. Por favor intentalo de nuevo!'));

            break;

        case 'categoria':

            if ($databaseManager->save(TABLE_CATEGORIAS, $_POST)) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;



        case 'blog':
            $_REQUEST['usuario'] = $USER;
            if ($last_inserted = $databaseManager->save(TABLE_BLOG, $_REQUEST)) {
                if (isset($_REQUEST['archivo']))
                    foreach ($_REQUEST['archivo'] as $imagen) {
                        $datos = [
                            'archivo'  => $imagen,
                            'blog' => $last_inserted,
                        ];
                        $databaseManager->save(TABLE_BLOG_IMAGENES, $datos);
                    }
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

    }

}

?>