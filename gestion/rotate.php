<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
// Archivo y rotación
$nombre_archivo = "images/blogmanagement/" . $_REQUEST['carpeta'] . "/big/" . trim($_REQUEST['imagen']);
$nombre_archivo_thumb = "images/blogmanagement/" . $_REQUEST['carpeta'] . "/thumb/" . trim($_REQUEST['imagen']);
$grados = $_REQUEST['grados'];
$imageFileType = pathinfo($nombre_archivo_thumb, PATHINFO_EXTENSION);
//
// Tipo de contenido
header('Content-type: image/jpeg');
clearstatcache();

if ($imageFileType == 'jpg' || $imageFileType == 'jpeg') {
// Cargar
    $origen = imagecreatefromjpeg($nombre_archivo_thumb);

// Rotar
    $rotar = imagerotate($origen, $grados, 0);

// Imprimir
    if (imagejpeg($rotar, $nombre_archivo) && imagejpeg($rotar, $nombre_archivo_thumb))
        echo trim($_REQUEST['imagen']);
    // Liberar la memoria
    imagedestroy($origen);
    imagedestroy($rotar);
} else {
    // Cargar
    $origen = imagecreatefrompng($nombre_archivo_thumb);

// Rotar
    $rotar = imagerotate($origen, $grados, 0);

// Imprimir
    if (imagepng($rotar, $nombre_archivo) && imagepng($rotar, $nombre_archivo_thumb))
        echo trim($_REQUEST['imagen']);
    // Liberar la memoria
    imagedestroy($origen);
    imagedestroy($rotar);
}


?>
