<?php
include_once 'class/Main.class.php';

$file = $_FILES['imagen'];
$target_dir = 'images/blogmanagement/' . $_REQUEST['carpeta'] . '/big/';
$target_dir_thumb = 'images/blogmanagement/' . $_REQUEST['carpeta'] . '/thumb/';
$target_file = $target_dir . basename($file["name"]);
$target_file_thumb = $target_dir_thumb . basename($file["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
$unique_file_name = uniqid() . '.' . trim($imageFileType);
// Check if image file is a actual image or fake image
if (isset($_POST["submit"])) {
    $check = getimagesize($file["tmp_name"]);
    if ($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
//if (file_exists($target_file)) {
//    echo "Sorry, file already exists.";
//    $uploadOk = 0;
//}
// Check file size
if ($file["size"] > 300000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
//if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
//    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
//    $uploadOk = 0;
//}
//// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file

    // Crear una imagen en blanco y añadir algún texto


} else {
//    $image = imagecreatefromstring(file_get_contents($file['tmp_name']));
//    $exif = exif_read_data($file['tmp_name']);
//    if (!empty($exif['Orientation'])) {
//        switch ($exif['Orientation']) {
//            case 8:
//                $image = imagerotate($image, 90, 0);
//                break;
//            case 3:
//                $image = imagerotate($image, 180, 0);
//                break;
//            case 6:
//                $image = imagerotate($image, -90, 0);
//                break;
//        }
//    }
//    imagejpeg($image, $target_dir . $unique_file_name);
// $image now contains a resource with the image oriented correctly
    if (move_uploaded_file($file["tmp_name"], $target_dir . $unique_file_name)) {
        Main::generate_crop($unique_file_name, 1440, 900, $target_dir, $target_dir_thumb);
        echo trim($unique_file_name);
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>