</div>
</div>
<!--BOWER COMPONENTS-->
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- BootstrapValidator 3.3.6 -->
<script src=" <?= DIR_WS_BOWER_COMPONENTS ?>bootstrapvalidator/dist/js/bootstrapValidator.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/daterangepicker/daterangepicker.js"></script>
<!-- fullcalendar -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- colorpicker -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- timepicker -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- datepicker -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
<!-- iCheck -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/iCheck/icheck.js"></script>
<!-- Slimscroll -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/fastclick/fastclick.js"></script>

<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/ionslider/ion.rangeSlider.min.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<!-- AdminLTE App -->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>AdminLTE/dist/js/app.min.js"></script>

<!--	Fancybox	-->
<link rel="stylesheet" type="text/css" href="<?= DIR_WS_BOWER_COMPONENTS ?>fancybox/source/jquery.fancybox.css"
      media="screen"/>
<script type="text/javascript" src="<?= DIR_WS_BOWER_COMPONENTS ?>fancybox/source/jquery.fancybox.js"></script>

<!--	TINYMCE (Richtext editor)	-->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>/tinymce/tinymce.min.js"></script>

<!--	Select2	-->
<!--<script src="--><? //= DIR_WS_BOWER_COMPONENTS ?><!--AdminLTE/plugins/select2/select2.full.min.js"></script>-->
<script src="<?= DIR_WS_JS ?>select2/select2.full.min.js"></script>

<!--	jsPDF	-->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>jspdf/dist/jspdf.min.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>jspdf/dist/jspdf.plugin.autotable.js"></script>
<!--	Bootstrap - Table	-->
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>bootstrap-table/dist/bootstrap-table.min.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>bootstrap-table/dist/locale/bootstrap-table-es-AR.min.js"></script>
<script
    src="<?= DIR_WS_BOWER_COMPONENTS ?>bootstrap-table/dist/extensions/export/bootstrap-table-export.min.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>bootstrap-table/dist/extensions/export/tableExport.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>jQuery-File-Upload-9.12.6/js/vendor/jquery.ui.widget.js"></script>
<script src="<?= DIR_WS_BOWER_COMPONENTS ?>jQuery-File-Upload-9.12.6/js/jquery.iframe-transport.js"></script>

<!--- Sweetalert --->
<script src="<?= DIR_WS_JS ?>plugins/sweetalert/sweetalert.min.js"></script>
<!--- Summernote --->
<script src="<?= DIR_WS_JS ?>plugins/summernote/summernote.min.js"></script>
<script src="<?= DIR_WS_JS ?>plugins/summernote/lang/summernote-es-ES.js"></script>
<!--	index.js - General functions	-->
<script src="<?= DIR_WS_JS ?>index.js"></script>
<?php
if (!empty($_GET["page"]))
    echo '<script type="text/javascript" src="' . DIR_WS_JS_PAGES . $_GET["page"] . '.js"></script>';
?>


</body>
</html>