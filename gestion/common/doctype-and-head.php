<?
$nombre = utf8_encode($NAME);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title><?= TITLE." | " . $nombre ?> </title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= HTTP_SERVER ?>favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="<?= DIR_WS_CSS ?>select2/select2.min.css">
    <link rel="stylesheet" href="<?= DIR_WS_CSS ?>select2/select2-bootstrap.min.css">
    <link rel="stylesheet" href="<?= DIR_WS_CSS ?>dropzone/dropzone.css">
    <link rel="stylesheet" href="<?= DIR_WS_CSS ?>style.css">
    <!-- Bootstrap time Picker -->
    <script src="<?= DIR_WS_BOWER_COMPONENTS ?>jquery/dist/jquery.min.js"></script>
</head>

<body class="skin-yellow-light skin-custom fixed sidebar-mini">
<div class="wrapper">
<!--==============================HEADER=================================-->

