<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Configuration extends Main
{
    //constructor
    var $dbConnection;
    var $table = TABLE_CONFIGURATION;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function insertar($campos)
    {

        if ($connection = $this->dbConnection->connect()) {
            $fields = $this->selectFieldTable();
            $query = "INSERT INTO " . $this->table . " (";
            while ($field = mysql_fetch_array($fields)):
                $query .= $field[0] . ", ";
            endwhile;
            $query = substr($query, 0, strlen($query) - 2);
            $query .= ") VALUES (";
            foreach ($campos as $campo):
                $query .= "'" . $campo . "', ";
            endforeach;
            $query = substr($query, 0, strlen($query) - 2);
            $query .= ")";
            return mysql_query($query);
        }

    }

    public function update($values, $id)
    {
        if ($connection = $this->dbConnection->connect()) {
            $fields = $this->selectFieldTable($this->table, array('Id', 'variable'));
            $query = "UPDATE " . $this->table . " SET ";
            $i = 0;
            foreach ($fields as $field) {
                $query .= $field['Field'] . " = '" . $values[$i] . "', ";
                $i++;
            }
            $query = substr($query, 0, strlen($query) - 2);
            $query .= " WHERE Id = " . $id;
            return $connection->query($query);
        }
    }


    public function delete($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            return mysql_query("DELETE FROM " . $this->table . " WHERE Id = " . $id);
        }
    }

    public function listar($rows, $sort, $order, $offset, $field, $search)
    {
        if ($connection = $this->dbConnection->connect()) {
            $where = "";
            if (!empty($search)) {
                $where .= " AND " . $field . " LIKE '%$search%'";
            }

            $query = $connection->query("SELECT *
								 FROM " . $this->table . "
								 WHERE  Id != 0 $where 
								 ORDER BY $sort $order LIMIT $offset,$rows");
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function contar($field, $search)
    {
        if ($connection = $this->dbConnection->connect()) {
            $where = "";
            if (!empty($search)) {
                $where .= " AND " . $field . " LIKE '%$search%'";
            }
            $query = $connection->query("SELECT COUNT(*)
								 FROM " . $this->table . "
								 WHERE  Id != 0  $where");
            $row = $query->fetch();
            return $row[0];
        }


    }

    public function getConfigurationValues()
    {
        if ($connection = $this->dbConnection->connect()) {

            $query = $connection->query("SELECT *
								 FROM " . $this->table . "
								 WHERE  Id != 0");
            $results = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($results as $result)
                $return[$result['variable']] = $result['value'];
            return $return;
        }
    }


}

?>