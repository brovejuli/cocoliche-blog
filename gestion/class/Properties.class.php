<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Properties extends Main
{
    //constructor
    var $dbConnection;
    var $table = TABLE_PROPERTIES;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function select()
    {
        if ($connection = $this->dbConnection->connect()) {

            $query = $connection->query("
                                SELECT *,pr.province as province, c.city as city  
                                FROM " . $this->table . " p
                                JOIN ".TABLE_CITIES." c ON c.Id = p.city
                                JOIN ".TABLE_PROVINCES." pr ON pr.Id = p.province
                                ORDER BY created_at DESC
                                ") ;
            if ($query)
                return $query->fetchAll(PDO::FETCH_ASSOC);
            else
                return false;
        }
    }

}

?>