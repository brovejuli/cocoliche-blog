<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Fotos extends Main
{
    //constructor
    var $dbConnection;
    var $table = TABLE_FOTOS;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function select($id_obra = null)
    {
        if ($connection = $this->dbConnection->connect()) {

            if($id_obra)
                $and = " AND f.obra = $id_obra ";
            $query = $connection->query("
                                SELECT 
                                  f.*, 
                                  o.nombre as obra,
                                  f.obra as id_obra,
                                  fg.imagen,
                                  fg.Id as id_imagen
                                FROM " . TABLE_FOTOS_GALERIA . " fg
                                JOIN " . TABLE_FOTOS . " f ON f.Id = fg.id_foto
                                JOIN " . TABLE_OBRAS . " o ON o.Id = f.obra
                                WHERE fg.borrar = 0
                                $and
                                ORDER BY fecha DESC
                                ");
            if ($query)
                return $query->fetchAll(PDO::FETCH_ASSOC);
            else
                return false;
        }
    }

}

?>