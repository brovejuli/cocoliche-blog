<?php
define('__ROOTSERVER__', dirname(dirname(__FILE__)));
include_once(__ROOTSERVER__ . '/class/Connection.class.php');
require_once(__ROOTSERVER__ . '/includes/database_tables.php');
require_once(__ROOTSERVER__ . '/class/ResizeImage.class.php');
require_once(__ROOTSERVER__ . '/class/Main.class.php');

class DatabaseManagerFrontEnd extends Main
{
    var $dbConnection;
    public $databaseTables = [];

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function getSliderItems()
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("
                                SELECT b.*, bi.archivo as imagen
                                FROM " . TABLE_BLOG . " b
                                JOIN " . TABLE_BLOG_IMAGENES . " bi ON bi.blog = b.Id
                                ORDER BY b.fecha DESC
                                ");

            return self::query_converter($query->fetchAll(PDO::FETCH_ASSOC));
        }
    }

}

?>