<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Users extends Main
{
    //constructor
    var $dbConnection;
    var $table = TABLE_USERS;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function select($rows, $sort, $order, $offset, $field, $search)
    {
        if ($connection = $this->dbConnection->connect()) {
            $where = "";
            if (!empty($search))
                $where .= " WHERE " . $field . " LIKE '%$search%'";

            $query = $connection->query("SELECT u.*, l.level as level_name, u.level FROM " . $this->table . " u
                                JOIN " . TABLE_LEVELS . " l ON l.Id = u.level
                                $where
                                ORDER BY $sort $order
                                LIMIT $offset,$rows
                                ");
            if ($query)
                return $query->fetchAll(PDO::FETCH_ASSOC);
            else
                return false;
        }
    }

}

?>