<?php
define('__ROOTSERVER__', dirname(dirname(__FILE__)));
include_once(__ROOTSERVER__ . "/class/Connection.class.php");
require_once(__ROOTSERVER__ . "/includes/database_tables.php");
require_once(__ROOTSERVER__ . "/class/ResizeImage.class.php");

class Main
{
    var $dbConnection;


    function __construct()
    {
        $this->dbConnection = new DBManager;
    }


    public function mostrarMenu($a = 0)
    {
        if ($connection = $this->dbConnection->connect()) {
            if ($a == 0)
                return $connection->query("SELECT SUBSTRING(Id, 1,1) AS Id, Nombre, Grupo, Titulo, Link, iconCls FROM " . TABLE_MENU . " WHERE CHAR_LENGTH(Id) = 1 ");
            else
                return $connection->query("SELECT SUBSTRING(Id, 1,2) AS Id, Nombre, Grupo, Titulo, Link, iconCls, level FROM " . TABLE_MENU . " WHERE CHAR_LENGTH(Id) = 1 AND level = " . $a);
        }
    }

    public function mostrarSubmenu($n, $a)
    {
        if ($connection = $this->dbConnection->connect()) {
            if ($a == 0)
                return $connection->query("SELECT SUBSTRING(Id, 3,1) AS Id, Link, Nombre, Titulo, iconCls, level FROM " . TABLE_MENU . " WHERE SUBSTRING(Id, 1,1) = " . $n . " AND CHAR_LENGTH(Id) = 3 ORDER BY Id");
            else
                return $connection->query("SELECT SUBSTRING(Id, 4,1) AS Id, Link, Nombre, Titulo, iconCls, level FROM " . TABLE_MENU . " WHERE SUBSTRING(Id, 1,2) = " . $n . " AND CHAR_LENGTH(Id) = 3 AND level = " . $a . "  ORDER BY Id");
        }
    }

    public function contarSubmenu($n, $a)
    {
        if ($connection = $this->dbConnection->connect()) {
            if ($a == 0)
                $query = "SELECT COUNT(Id) AS num FROM " . TABLE_MENU . " WHERE SUBSTRING(Id, 1,1) = " . $n . " AND CHAR_LENGTH(Id) = 3 ";
            else
                $query = "SELECT COUNT(Id) AS num FROM " . TABLE_MENU . " WHERE SUBSTRING(Id, 1,1) = " . $n . " AND CHAR_LENGTH(Id) = 3 AND level = " . $a . "";
            $sql = $connection->query($query);
            $row = $sql->fetch();
            return $row[0];
        }
    }

    public function level_file($link, $a)
    {
        if ($connection = $this->dbConnection->connect()) {
            if ($a != 0)
                $sql = $connection->query("SELECT Count(Id) FROM " . TABLE_MENU . " WHERE level = " . $a . " AND Link = '" . $link . "'");
            else
                $sql = $connection->query("SELECT Count(Id) FROM " . TABLE_MENU . " WHERE Link = '" . $link . "'");
            $row = $sql->fetch();
            if ($row[0] > 0)
                return true;
            else
                return false;
        }
    }

    public function users_levels()
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT * FROM " . TABLE_LEVELS . " ORDER BY Id");
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    static public function pagination($page, $tpages, $show)
    {

        $pagination = '';
        $first = intval($show / 2);
        $last = ($show % 2) ? $first * 2 : ($first * 2 - 1);

        $start = $page - $first;

        if ($start <= 0)
            $start = 1;

        $end = ($tpages < $show) ? $tpages : $start + $last;

        if ($end > $tpages) {
            $start = $start - ($end - $tpages);
            $end = $tpages;
        }

        $pagination .= "<script>
							$('#pagination .pagination-page').click(function(e){
								e.preventDefault();
								page = $(this).data('page');
								listar();
								page = '1';
							});
							
							$('#prev').click(function(e){
							    e.preventDefault();
								page = parseInt($('#pagination input[type=radio]:checked').val()) - 1;
								listar();
								page = '1';
								});
								
							$('#next').click(function(e){
							    e.preventDefault();
								page = parseInt($('#pagination input[type=radio]:checked').val()) + 1;
								listar();
								page = '1';
								});

					   </script>";

        $hide = ($start > 1) ? '' : 'style="visibility:hidden"';
        $pagination .= '';

        $pagination .= '<ul class="pagination pagination-sm no-margin pull-right"><li><a href="#" id="prev">«</a></li>';

        $pagination .= '';

        for ($i = $start; $i <= $end; $i++) {
            $active = ($i == $page) ? 'class="active"' : '';
            $checked = ($i == $page) ? 'checked' : '';
            $pagination .= '<li ' . $active . '><a data-page="' . $i . '" href="#" class="pagination-page active" name="page" id="' . $i . '"value="' . $i . '" ' . $checked . '>' . $i . '</a></li>';
        }

        $pagination .= '</div></div>';

        $hide = ($end < $tpages) ? '' : 'style="visibility:hidden"';
        $pagination .= '';
        $pagination .= '<li><li><a href="#" id="next">»</a></li></li></ul>';
        return $pagination;

    }


    /**
     * @param $fecha (yyyy-mm-dd)
     * @return string
     */
    static public function changeDSqlToNormal($date)
    {
        list ($y, $m, $d) = explode("-", explode(' ', $date)[0]);
        return $d . "/" . $m . "/" . $y . ' ' . explode(' ', $date)[1];
    }

    /**
     * @param $fecha (dd/mm/yyyy)
     * @return string
     */
    static public function changeDNormalToSql($fecha)
    {
        list($d, $m, $y) = explode("/", $fecha);
        return $y . '-' . $m . '-' . $d;
    }

    /**
     * @param $values array
     * @return integer
     */
    public function register_log($values)
    {
        if ($connection = $this->dbConnection->connect()) {

            /*Fields where insert*/
            $fields = $this->selectFieldTable(TABLE_PROCESS_LOGS, array('Id', 'date'));
            foreach ($fields as $field) {
                $insert_fields[] = $field['Field'];
            }
            $insert_fields = '(' . implode(',', $insert_fields) . ')';

            /*Values to insert*/
            $values = "'" . implode("','", $values) . "'";

            /*Query*/
            $connection->query("INSERT INTO " . TABLE_PROCESS_LOGS . $insert_fields . " VALUES (" . $values . ") ");
            return $connection->lastInsertId();
        }
    }

    /**
     * @param $array -> receives a array whit data from database and transform every element in utf8 format.
     * @return $array -> utf8 formated array.
     */
    static public function utf8_converter($array)
    {
        array_walk_recursive($array, function (&$item, $key) {
            if (!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            }
        });

        return $array;
    }

    /**
     * @param $array -> receives a array whit data from database and transform every element in utf8 format.
     * @return $array -> utf8 formated array.
     */
    static public function query_converter($array)
    {
        array_walk_recursive($array, function (&$item, $key) {
            if (!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            } elseif (Main::is_date($item)) {
                $item = Main::changeDSqlToNormal($item);
            } elseif ((($item == 0 || $item == 1) && is_numeric($item) && $key != 'Id' && $key != 'obra' && $key != 'id_obra'))
                $item = Main::is_bool($item);
            elseif ($key == 'archivo')
                $item = '<img src="' . DIR_WS_IMAGES_BLOG_THUMB . $item . '" width="100px">';
        });

        return $array;
    }

    /**
     * @param $image
     * @param $width
     * @param $height
     * @param $sourceDirectory
     * @param $destinationDirectory
     */
    static public function generate_crop($image, $width, $height, $sourceDirectory, $destinationDirectory)
    {

        $image_name = $image;

        $Quality = 100;

        // *** 1) Initialize / load image
        $resizeObj = new ResizeImage($sourceDirectory . $image);
        // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
        $resizeObj->resizeImage($width, $height, 'crop');
        // *** 3) Save image
        $resizeObj->saveImage($destinationDirectory . $image, $Quality);
    }

    static public function upload_image($image, $width, $height, $sourceDirectory, $destinationDirectory)
    {

        $image_name = $image;

        $Quality = 100;

        // *** 1) Initialize / load image
        $resizeObj = new ResizeImage($image);
        // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
        $resizeObj->resizeImage($width, $height, 'auto');
        // *** 3) Save image
        $resizeObj->saveImage($destinationDirectory . $image, $Quality);
    }

    static public function is_date($str)
    {
        $str = str_replace('/', '-', $str);
        $stamp = strtotime($str);
        if (is_numeric($stamp)) {
            $month = date('m', $stamp);
            $day = date('d', $stamp);
            $year = date('Y', $stamp);
            return checkdate($month, $day, $year);
        }
        return false;
    }

    /**
     * @param string $string
     * @return bool
     */
    public function is_timestamp($string)
    {
        try {
            new DateTime('@' . $string);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }


    static public function is_bool($str)
    {

        if ($str == 1)
            return 'Si';
        elseif ($str == 0)
            return 'No';
        else
            return $str;
    }

    public static function clean_string($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

}

?>