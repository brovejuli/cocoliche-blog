<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require __ROOT__ . '/class/mailer/class.phpmailer.php';
require __ROOT__ . '/class/mailer/class.smtp.php';
include_once(__ROOT__ . "/class/Connection.class.php");
include_once(__ROOT__ . "/class/Configuration.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Email extends Main
{
    //constructor
    var $dbConnection;
    var $table = TABLE_EMAILS;


    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function send_mail($emails, $data, $subject, $message)
    {
        error_reporting(0);
        $dbManager = new DatabaseManager();
        $objConfiguration = new Configuration();
        $configuration = $objConfiguration->getConfigurationValues();

        $mail_from = $configuration['Mail_From'];
        $mail_pass = $configuration['Mail_Password'];
        $mail_name = $configuration['Mail_From_Name'];
        $mail_host = $configuration['MailHost'];

        $mail = new PHPMailer;

        $mail->SMTPDebug = 0;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $mail_host;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $mail_from;                 // SMTP username
        $mail->From = $mail_from;                 // SMTP username
        $mail->Password = $mail_pass;                          // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom($mail_from, $mail_name);
        foreach ($emails as $email)
            $mail->addAddress($email, $email);     // Add a recipient
        $mail->addReplyTo('no-reply@figueredoarquitectura.com.ar', $_POST['name']);
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = utf8_decode($subject);
        $message = $message;
        $message .= $configuration['Empresa_www'];
        $mail->Body = utf8_decode($message);
//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if (!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            $dbManager->save(TABLE_NEWSLETTER, $_POST);
            return false;
        } else {
//            $dbManager->save(TABLE_NEWSLETTER, $_POST);
//            echo 'Message has been sent';
            return true;
        }

    }

}

?>