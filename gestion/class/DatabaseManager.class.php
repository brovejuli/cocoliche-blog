<?php
define('__ROOTSERVER__', dirname(dirname(__FILE__)));
include_once(__ROOTSERVER__ . "/class/Connection.class.php");
require_once(__ROOTSERVER__ . "/includes/database_tables.php");
require_once(__ROOTSERVER__ . "/class/ResizeImage.class.php");
require_once(__ROOTSERVER__ . "/class/Main.class.php");

class DatabaseManager
{
    var $dbConnection;
    public $databaseTables = [];

    function __construct()
    {
        $this->dbConnection = new DBManager;
        $this->databaseTables = $this->showTables();
    }

    /**
     * @return array with database tables names with integer index
     */
    function showTables()
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SHOW tables FROM " . $this->dbConnection->getDatabaseName());
            return $query->fetchAll(PDO::FETCH_NUM);
        }
    }

    /**
     * @param $table -> the table to display fields
     * @param null $exceptions -> fields to not show in the query
     * @return array -> fields and types from table selected
     */
    public function selectFieldTable($table, $exceptions = null)
    {
        if ($connection = $this->dbConnection->connect()) {
            if ($exceptions) {
                $exceptions = "'" . implode("','", $exceptions) . "'";
                $where = " WHERE Field NOT IN (" . $exceptions . ") ";
            }
            $query = $connection->query("SHOW COLUMNS  FROM " . DB_DATABASE . "." . $table . $where);
            $results = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($results as $result)
                $return[$result['Field']] = $result['Type'];
            return $return;
        }
    }

    /**
     * @param $fields
     * @param $values
     * @return integer
     */
    public function insert($table, $fields, $values)
    {
        if ($connection = $this->dbConnection->connect()) {
            foreach ($fields as $field) {
                $insert_fields[] = $field['Field'];
            }
            $insert_fields = '(' . implode(',', $insert_fields) . ')';
            $values = "'" . implode("','", $values) . "'";
            if ($connection->query("INSERT INTO " . $table . $insert_fields . " VALUES (" . $values . ") "))
                return $connection->lastInsertId();
            else
                return $connection->errorInfo();
        }
    }

    public function save($table, $form_data)
    {
        if ($connection = $this->dbConnection->connect()) {
            $dbFields = $this->selectFieldTable($table, ['Id', 'borrar', 'id']);
            foreach ($form_data as $field => $value) {
                if (array_key_exists($field, $dbFields)) {
                    $fields[] = $field;
                    if ($dbFields[$field] == 'pass')
                        $values[] = md5($value);
                    elseif ($dbFields[$field] == 'date')
                        $values[] = Main::changeDNormalToSql($value);
                    else
                        $values[] = addslashes(utf8_decode($value));
                }
            }
            $insert_fields = '(' . implode(',', $fields) . ')';
            $values = "'" . implode("','", $values) . "'";
            if ($connection->query("INSERT INTO " . $table . $insert_fields . " VALUES (" . $values . ") "))
                return $connection->lastInsertId();
            else
                return $connection->errorInfo();
        }
    }

    public function remove($table, $id, $field = null)
    {

        $where = $field ? " WHERE $field = '$id'" : " WHERE Id = $id";

        if ($connection = $this->dbConnection->connect()) {
            return $connection->query("DELETE FROM $table $where")
            or die(var_dump($connection->errorInfo()));
        }

    }

    public function disable($table, $id, $field = null)
    {
        $where = $field ? " WHERE $field = '$id'" : " WHERE Id = $id";
        if ($connection = $this->dbConnection->connect()) {
            return $connection->query("UPDATE $table SET borrar = 1 $where");
        }
    }

    public function select($table, $sort, $order, $borrar = null, $id = null, $field = null)
    {
        if ($connection = $this->dbConnection->connect()) {
            $where = $borrar ? " AND borrar = 0 " : '';
            if ($field)
                $and = $id ? " AND $field = $id " : '';
            else
                $and = $id ? " AND Id = $id " : '';
            $query = $connection->query("
                                SELECT * 
                                FROM $table
                                WHERE 1
                                $where
                                $and
                                ORDER BY $sort $order
                                ");

            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function count($table, $field, $search)
    {
        if ($connection = $this->dbConnection->connect()) {
            $where = "";
            if (!empty($search)) {
                $where .= " AND " . $field . " LIKE '%$search%'";
            }

            $sql = $connection->query("SELECT count(*) FROM " . $table . " WHERE borrar = 0 $where");
            if (!$sql)
                $sql = $connection->query("SELECT count(*) FROM " . $table . " $where");
            if ($sql) {
                $row = $sql->fetch();
                return $row[0];
            } else
                return $connection->errorInfo();
        }
    }

    public function update($table, $form_data)
    {
        if ($connection = $this->dbConnection->connect()) {
            $dbFields = $this->selectFieldTable($table, ['Id', 'borrar', 'id']);
            foreach ($form_data as $field => $value) {
                if (array_key_exists($field, $dbFields)) {
                    if ($field != 'action' && $field != 'Id') {
                        if ($field == 'pass')
                            $update_values[] = " $field = '" . md5($value) . "' ";
                        elseif ($dbFields[$field] == 'date')
                            $update_values[] = " $field = '" . Main::changeDNormalToSql($value) . "' ";
                        elseif ($field == 'resource') {
                            if (file_exists('../material/' . Main::clean_string($value) . '.' . pathinfo($value, PATHINFO_EXTENSION)))
                                $update_values[] = " $field = '" . Main::clean_string($value) . '.' . pathinfo($value, PATHINFO_EXTENSION) . "' ";
                            else
                                $update_values[] = " $field = 'http://" . preg_replace('#^https?://#', '', $value) . "'";
                        } else
                            $update_values[] = " $field = '" . addslashes(utf8_decode($value)) . "' ";
                    }
                }
            }
            $update_data = implode(',', $update_values);
            if ($connection->query("UPDATE " . $table . " SET " . $update_data . " WHERE Id = " . $form_data['Id']) or die(var_dump($connection->errorInfo())))
                return true;
            else
                return $connection->errorInfo();
        }
    }
}

?>