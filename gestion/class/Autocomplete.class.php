<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Autocomplete extends Main
{

    //Variables
    var $dbConnection;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function provinces()
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT *
                                FROM " . TABLE_PROVINCES . "
                                ");
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function cities($province)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT *
                                FROM " . TABLE_CITIES . "
                                WHERE province = $province
                                ");
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function property($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT *
                                FROM " . TABLE_PROPERTIES . " tp
                                WHERE Id = $id
                                ") or die(var_dump($connection->errorInfo()));
            return $query->fetch(PDO::FETCH_ASSOC);
        }
    }

    public function property_images($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT image
                                FROM " . TABLE_PROPERTIES_IMAGES . "
                                WHERE property = $id
                                ");
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }


}

?>