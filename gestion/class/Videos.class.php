<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Videos extends Main
{
    //constructor
    var $dbConnection;
    var $table = TABLE_VIDEOS;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function select()
    {
        if ($connection = $this->dbConnection->connect()) {

            $query = $connection->query("
                                SELECT 
                                  f.*, 
                                  o.nombre as obra,
                                  f.obra as id_obra,
                                  (SELECT COUNT(Id) FROM videos_galeria WHERE id_video = f.Id) as imagenes
                                FROM " . $this->table . " f
                                JOIN " . TABLE_OBRAS . " o ON o.Id = f.obra
                                WHERE f.borrar = 0
                                ORDER BY fecha DESC
                                ");
            if ($query)
                return $query->fetchAll(PDO::FETCH_ASSOC);
            else
                return false;
        }
    }

}

?>