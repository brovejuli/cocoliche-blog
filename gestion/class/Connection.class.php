<?php

define('__ROOTCNX__', dirname(dirname(__FILE__)));
require_once(__ROOTCNX__ . "/includes/configure.php");

class DBManager
{
    var $connection;

    var $db;
    var $server;
    var $user;
    var $pass;

    var $connection_error;

    /**
     * DBManager constructor.
     * @param $db
     * @param $server
     * @param $user
     * @param $pass
     */
    public function __construct()
    {
        $this->db = DB_DATABASE;
        $this->server = DB_SERVER;
        $this->user = DB_SERVER_USERNAME;
        $this->pass = DB_SERVER_PASSWORD;
    }

    function connect()
    {
        try {
            $this->connection = new PDO('mysql:host=' . $this->server . ';dbname=' . $this->db, $this->user, $this->pass);
        } catch (PDOException $e) {
            $this->connection_error = $e->getMessage();
        }
        return $this->connection;
    }

    public function getConnectionError()
    {
        return $this->connection_error;
    }

    public function getDatabaseName(){
        return $this->db;
    }
}

?>



