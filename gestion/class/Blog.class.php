<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Blog extends Main
{
    //constructor
    var $dbConnection;
    var $table = TABLE_BLOG;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function select()
    {
        if ($connection = $this->dbConnection->connect()) {

            $query = $connection->query("
                                SELECT 
                                 b.*,
                                 bi.archivo
                                FROM " . $this->table . " b
                                JOIN " . TABLE_BLOG_IMAGENES . " bi ON bi.blog = b.Id
                                WHERE b.borrar = 0
                                ORDER BY fecha DESC
                                ");
            if ($query)
                return $query->fetchAll(PDO::FETCH_ASSOC);
            else
                return false;
        }
    }

}

?>