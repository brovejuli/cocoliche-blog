<!--==============================CONTENT=================================-->
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">
            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3><i class="fa fa-user"></i> <?= $_GET["title"] ?></h3>
                        </div>
                        <div class="box-body table-responsive table-warning">
                            <div id="toolbar" class="btn-group">
                                <button type="button" class="btn btn-default" onclick="doNew()"
                                        data-toggle="modal" data-target="#modal_form">
                                    <i class="glyphicon glyphicon-plus"></i> Nuevo
                                </button>
                            </div>
                            <table class="table table-hover table-striped data-table"
                                   data-toggle="table"
                                   data-url="listar.php?action=users"
                                   data-query-params="queryParams"
                                   data-pagination="true"
                                   data-search="true"
                                   data-height="600"
                                   data-click-to-select="true"
                                   data-show-refresh="true"
                                   data-show-toggle="true"
                                   data-show-columns="true"
                                   data-toolbar="#toolbar">
                                <thead>
                                <tr>
                                    <th data-field="name" data-sortable="true">Nombre</th>
                                    <th data-field="user" data-sortable="true">Usuario</th>
                                    <th data-field="level_name" data-sortable="true">Nivel</th>
                                    <th data-field="action" data-formatter="actionFormatter" data-events="actionEvents"
                                        class="col-md-2">
                                        Acciones
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="box-footer clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>

            </div>
            <form id="frm" method="post" class="form-horizontal">
                <input type="hidden" name="action" value="user"/>
                <input type="hidden" name="Id" id="Id"/>
                <div class="hidden-update-data"></div>
                <div class="border row">

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Nombre</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span
                                            class="glyphicon glyphicon-pencil"></span></span>
                                    <input type="text" id="name" name="name" class="form-control clearval"
                                           placeholder="Input name" aria-describedby="basic-addon1">
                                    <input type="hidden" id="Id" name="Id"/>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">Usuario</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"
                                                                                         aria-hidden="true"></i></span>
                                    <input type="text" id="user" name="user" class="form-control clearval"
                                           placeholder="Input user" aria-describedby="basic-addon1"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">Nivel</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"
                                                                                         aria-hidden="true"></i></span>
                                    <select id="level" name="level" class="form-control clearval"
                                            aria-describedby="basic-addon1">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger alert_form" role="alert"></div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Password-->
<div class="modal fade modal-add" id="modal_pass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title-pass" id="myModalLabel">Password</h4>

            </div>
            <form id="frmPass" method="post" class="form-horizontal">
                <input type="hidden" name="action" value="users_pass"/>
                <div class="border row">

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"
                                                                                         aria-hidden="true"></i></span>
                                    <input type="password" id="pass" name="pass" class="form-control clearval"
                                           placeholder="Input password" aria-describedby="basic-addon1">
                                    <input type="hidden" id="Id" name="Id"/>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">Repeat Password</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"
                                                                                         aria-hidden="true"></i></span>
                                    <input type="password" id="rpass" name="rpass" class="form-control clearval"
                                           placeholder="Repeat password" aria-describedby="basic-addon1"/>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="alert alert-danger alert_form" role="alert"></div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-custom btn-flat">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete !-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <p>¿Seguro que desea eliminar el usuario?</p>
                <p class="debug-url"></p>
                <form id="frmDelete" method="post">
                    <input type="hidden" name="action" value="user">
                    <input type="hidden" name="Id" id="Id" value="">
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger btn-ok" onclick="doRemove()">Aceptar</a>
            </div>
        </div>
    </div>
</div>