<?php
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
$dbManager = new DatabaseManager();
?>
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">

            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3 class="box-title">
                                <h1><i class="fa fa-list"></i> <?= $_GET["title"] ?></h1>
                            </h3>
                        </div>
                        <form id="frm">
                            <input type="hidden" name="action" id="action" value="blog">
                            <input type="hidden" name="Id" id="Id" value="<?= $_REQUEST['blog'] ?>">
                            <input type="hidden" id="update" value="<?= $_REQUEST['blog'] ? 1 : 0 ?>">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="fecha">Fecha</label>
                                                <input type="text" name="fecha" id="fecha" class="form-control"
                                                       required value="<?= date('d/m/Y') ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="titulo" class="text-uppercase">T&iacute;tulo</label>
                                                <input type="text" name="titulo" id="titulo" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="categorias">Categoria</label>
                                                <select name="categorias[]" id="categorias" class="form-control">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="copete" class="text-uppercase">Copete</label>
                                                <textarea name="copete" id="copete" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="cuerpo" class="text-uppercase">Cuerpo</label>
                                                <textarea name="cuerpo" id="cuerpo" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="imagenes">Im&aacute;genes</label>
                                                <input type="file" name="imagen" id="imagen" class="hidden"  accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*">
                                                <div class="clearfix"></div>
                                                <button type="button" class="btn btn-custom btn-flat" id="file-trigger">
                                                    <span class="fa fa-file-image-o"></span> Seleccionar archivo
                                                </button>
                                                <button type="button" class="btn btn-custom btn-flat" id="subir"><span class="fa fa-upload"></span> Subir
                                                </button>
                                                <div class="links"></div>
                                                <div class="files"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <div class="col-md-12">
                                    <div class="col-md-2 pull-right">
                                        <button type="submit" class="btn btn-lg btn-flat btn-custom btn-block"><span class="fa fa-save"></span> Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

