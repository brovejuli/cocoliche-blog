<div class="login-wrapper">
</div>
<body class="hold-transition login-page">

<div class="login-box">

    <!-- /.login-logo -->
    <div class="login-box-body">
        <div class="row">
            <div class="login-logo text-center">
                <img src="images/logo-sistema.png">
            </div>
            <p class="login-box-msg">Iniciar sesion</p>
            <form class="login" id="login" onSubmit="return false">
                <input type="hidden" id="redir" name="redir" value="">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Usuario">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" id="password" name="password"
                           placeholder="Contrase&ntilde;a">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">

                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <input type="submit" class="submit btn btn-primary btn-block btn-flat" value="Login">
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <hr>
            <div class="col-xs-12">
                <a href="http://www.powerbyte.com.ar/" target="_blank">
                    <div class="col-xs-3">
                        <img src="images/pb_logo.png" class="img-responsive">
                    </div>
                    <div class="col-xs-9 company-name text-center">
                        <span class="">Powered by Powerbyte Soluciones Inform&aacute;ticas</span>
                    </div>
                </a>
            </div>
        </div>
    </div>


</div>
<!-- /.login-box-body -->

</body>
<!-- /.login-box -->

