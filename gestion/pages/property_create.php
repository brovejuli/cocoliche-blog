<?php
include 'alerts.php';
include 'validacion.php';

?>

<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="box box-custom box-solid">
            <div class="box-header">
                <h3 class="box-title">
                    <h1><i class="fa fa-home"></i> <span>Nueva Publicaci&oacute;n</span></h1>
                </h3>
            </div>
            <div class="box-body">
                <form id="frm" method="post" class="col-md-12 hidden">
                    <input type="hidden" name="Id" id="Id" value="<?= $_GET['Id'] ?>"/>
                    <input type="hidden" name="user" id="user" value="<?= $IDUSER ?>"/>
                    <input type="hidden" name="action" id="action" value="property"/>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="price">Precio aviso</label>
                                <input type="text" name="price" id="price" class="form-control just-numbers" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="consult">Consultar precio</label>
                                <select name="consult" id="consult" class="form-control">
                                    <option value="1">Si</option>
                                    <option value="0" selected>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="money_type">Moneda</label>
                                <select name="money_type" id="money_type" class="form-control" required>
                                    <option value="$" selected>$</option>
                                    <option value="USD">USD</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="active">Propidad activa</label>
                                <select name="active" id="active" class="form-control">
                                    <option value="1" selected>Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="sortable">
                        <label for="slider_images" class="col-md-12 col-xs-12">Imagenes slider</label>
                        <div class="files"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="type">Tipo de propiedad</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="Bóveda">Bóveda</option>
                                    <option value="Cabaña">Cabaña</option>
                                    <option value="Campo">Campo</option>
                                    <option value="Casa">Casa</option>
                                    <option value="Casa en country">Casa en country</option>
                                    <option value="Casa quinta">Casa quinta</option>
                                    <option value="Chacra">Chacra</option>
                                    <option value="Chalet">Chalet</option>
                                    <option value="Cochera">Cochera</option>
                                    <option value="Countries">Countries</option>
                                    <option value="Barrio privado">Barrio privado</option>
                                    <option value="Departamento" selected>Departamento</option>
                                    <option value="Deposito">Deposito</option>
                                    <option value="Duplex">Duplex</option>
                                    <option value="Triplex">Triplex</option>
                                    <option value="Edificios">Edificios</option>
                                    <option value="Emprendimientos">Emprendimientos</option>
                                    <option value="Fincas">Fincas</option>
                                    <option value="Fondo de comercio">Fondo de comercio</option>
                                    <option value="Fracciones">Fracciones</option>
                                    <option value="Galpon">Galpon</option>
                                    <option value="Hotel">Hotel</option>
                                    <option value="Hosteria">Hosteria</option>
                                    <option value="Industriales">Industriales</option>
                                    <option value="Local">Local</option>
                                    <option value="Lote">Lote</option>
                                    <option value="Lote en country">Lote en country</option>
                                    <option value="Oficina">Oficina</option>
                                    <option value="Otros">Otros</option>
                                    <option value="Ph">Ph</option>
                                    <option value="Pisos">Pisos</option>
                                    <option value="Tambos">Tambos</option>
                                    <option value="Terreno">Terreno</option>
                                    <option value="Viñedo">Viñedo</option>
                                    <option value="Bodega">Bodega</option>
                                    <option value="28">Oficina</option>
                                    <option value="Otros">Otros</option>
                                    <option value="Pisos">Pisos</option>
                                    <option value="Tambos">Tambos</option>
                                    <option value="Terreno">Terreno</option>
                                    <option value="Viñedo">Viñedo</option>
                                    <option value="Bodega">Bodega</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="operation">Operacion</label>
                                <select name="operation" id="operation" class="form-control">
                                    <option value="alquiler">Alquiler</option>
                                    <!--                                    <option value="alquiler temporario">Alquiler temporario</option>-->
                                    <option value="venta" Selected>Venta</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="state">Estado</label>
                                <select name="state" id="state" class="form-control" required>
                                    <option value="disponible" selected>Disponible</option>
                                    <option value="alquilada">Alquilada</option>
                                    <option value="vendida">Vendida</option>
                                    <option value="reservada">Reservada</option>
                                    <option value="suspendida">Suspendida</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="suitable_bank">Apto banco</label>
                                <select name="suitable_bank" id="suitable_bank" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="suitable_professional">Apto profesional</label>
                                <select name="suitable_professional" id="suitable_professional" class="form-control"
                                >
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="province">Provincia</label>
                                <select name="province" id="province" class="form-control" required></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="city">Ciudad</label>
                                <select name="city" id="city" class="form-control" required></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="neighborhood">Direccion</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="neighborhood">Barrio</label>
                                <input type="text" name="neighborhood" id="neighborhood" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="street">Calle</label>
                                <input type="text" name="street" id="street" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="adress_location">Ubicacion</label>
                                <select name="adress_location" id="adress_location" class="form-control" required>
                                    <option value="E/">E/</option>
                                    <option value="Esq.">Esq./</option>
                                    <option value="Diag.">Diag./</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="adress">Calle/s</label>
                                <input type="text" name="adress" id="adress" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="height">Altura</label>
                                <input type="text" name="height" id="height" class="form-control just-numbers">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lot_size_width">Tamaño del lote</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" name="lot_size_width" id="lot_size_width"
                                       class="form-control just-numbers" placeholder="Ancho">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" name="lot_size_height" id="lot_size_height"
                                       class="form-control just-numbers" placeholder="Largo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="builded_surface">Superficie construida</label>
                                <input type="text" name="builded_surface" id="builded_surface"
                                       class="form-control just-numbers">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="lot_surface">Superficie lote</label>
                                <input type="text" name="lot_surface" id="lot_surface"
                                       class="form-control just-numbers">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="antiquity">Antiguedad</label>
                                <input type="text" name="antiquity" id="antiquity" class="form-control just-numbers">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="garage">Cochera</label>
                                <select name="garage" id="garage" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="garage_state">Estado</label>
                                <select name="garage_state" id="garage_state" class="form-control">
                                    <option value="Nuevo" selected>Nuevo</option>
                                    <option value="A estrenar">A estrenar</option>
                                    <option value="Regular">Regular</option>
                                    <option value="Bueno">Bueno</option>
                                    <option value="Muy Bueno">Muy Bueno</option>
                                    <option value="Excelente">Excelente</option>
                                    <option value="Usado">Usado</option>
                                    <option value="En construccion">En construccion</option>
                                    <option value="A demoler">A demoler</option>
                                    <option value="A Reciclar">A Reciclar</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dependency_service">Dependencia de servicio</label>
                                <select name="dependency_service" id="dependency_service" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="dorms">Dormitorios</label>
                                <input type="text" name="dorms" id="dorms" class="form-control just-numbers">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="terrace">Terraza</label>
                                    <select name="terrace" id="terrace" class="form-control">
                                        <option value="-1">Sin especificar</option>
                                        <option value="1">Si</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="balcony">Balcon</label>
                                <select name="balcony" id="balcony" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="heating_type">Tipo de calefaccion</label>
                                <select name="heating_type" id="heating_type" class="form-control">
                                    <option value="No posee" selected>No posee</option>
                                    <option value="Gas natural">Gas natural</option>
                                    <option value="Gas envasado">Gas envasado</option>
                                    <option value="Tiro balanceado">Tiro balanceado</option>
                                    <option value="Estufas Electrica">Estufas Electrica</option>
                                    <option value="Split Frio/Calor">Split Frio/Calor</option>
                                    <option value="Salamandra">Salamandra</option>
                                    <option value="Hogar a leña">Hogar a leña</option>
                                    <option value="Hogar a gas">Hogar a gas</option>
                                    <option value="Radiadores">Radiadores</option>
                                    <option value="Caldera">Caldera</option>
                                    <option value="Caldera individual">Caldera individual</option>
                                    <option value="Losa radiante">Losa radiante</option>
                                    <option value="Zocalo radiante">Zocalo radiante</option>
                                    <option value="Piso Radiante">Piso Radiante</option>
                                    <option value="Eskabes">Eskabes</option>
                                    <option value="Central">Central</option>
                                    <option value="Central por radiador">Central por radiador</option>
                                    <option value="Central por ducto">Central por ducto</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="luminosity">Luminosidad</label>
                                <select name="luminosity" id="luminosity" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="Excelente">Excelente</option>
                                    <option value="Bueno">Bueno</option>
                                    <option value="Muy bueno">Muy bueno</option>
                                    <option value="Oscuro">Oscuro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="expenses">Expensas</label>
                                <select name="expenses" id="expenses" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="security">Seguridad</label>
                                <select name="security" id="security" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="plant_location">Ubicacion planta</label>
                                <select name="plant_location" id="plant_location" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="Frente">Frente</option>
                                    <option value="Contrafrente">Contrafrente</option>
                                    <option value="Lateral">Lateral</option>
                                    <option value="Interno">Interno</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="floor">Piso</label>
                                <input type="text" name="floor" id="floor" class="form-control just-numbers">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="rooms">Ambientes</label>
                                <input type="text" name="rooms" id="rooms" class="form-control just-numbers">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="bathrooms">Baños</label>
                                <input type="text" name="bathrooms" id="bathrooms" class="form-control just-numbers">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="toilette">Toilette</label>
                                <input type="text" name="toilette" id="toilette" class="form-control just-numbers">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="yard">Patio</label>
                                <select name="yard" id="yard" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="orientation">Orientacion</label>
                                <select name="orientation" id="orientation" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="Norte" selected>Norte</option>
                                    <option value="Sur">Sur</option>
                                    <option value="Este">Este</option>
                                    <option value="Oeste">Oeste</option>
                                    <option value="Noreste">Noreste</option>
                                    <option value="Noroeste">Noroeste</option>
                                    <option value="Sureste">Sureste</option>
                                    <option value="Suroeste">Suroeste</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="park">Parque</label>
                                <select name="park" id="park" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="pool">Pileta</label>
                                <select name="pool" id="pool" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="quincho">Quincho</label>
                                <select name="quincho" id="quincho" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="gas">Gas natural</label>
                                <select name="gas" id="gas" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="light">Energia electrica</label>
                                <select name="light" id="light" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="sewers">Cloacas</label>
                                <select name="sewers" id="sewers" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="phone">Telefono</label>
                                <select name="phone" id="phone" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="water">Agua corriente</label>
                                <select name="water" id="water" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cable">Cable</label>
                                <select name="cable" id="cable" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="asphalt">Asfalto</label>
                                <select name="asphalt" id="asphalt" class="form-control">
                                    <option value="-1">Sin especificar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Descripcion</label>
                                <textarea name="description" id="description" class="form-control richtext-basic"
                                          required>
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="floors">Nro de pisos</label>
                                <input type="text" name="floors" id="floors" class="form-control just-numbers">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="floor_apartments">Dptos por piso</label>
                                <input type="text" name="floor_apartments" id="floor_apartments"
                                       class="form-control just-numbers">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="private_elevators">Ascensores privados</label>
                                <input type="text" name="private_elevators" id="private_elevators"
                                       class="form-control just-numbers">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="service_elevators">Ascensores de servicio</label>
                                <input type="text" name="service_elevators" id="service_elevators"
                                       class="form-control just-numbers">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-4">
                        <button class="btn btn-danger btn-flat btn-lg">Cancelar</button>
                        <button type="submit" class="btn btn-success btn-flat btn-lg savePostButton"
                                onclick="doNew(0);">
                            Guardar
                            y publicar
                        </button>
                    </div>
                </form>
                <div class="col-md-3 col-md-offset-4 loading-content">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3 class="box-title">Cargando...</h3>
                        </div>
                        <div class="box-body">
                            Cargando contenido, por favor espere...
                        </div>
                        <!-- /.box-body -->
                        <!-- Loading (remove the following to stop the loading)-->
                        <div class="overlay">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <!-- end loading -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </section>
</div>

