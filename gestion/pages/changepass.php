<script type="text/javascript" src="<?= DIR_WS_JS ?>/reveal/jquery-1.6.min.js"></script>
<!--==============================CONTENT=================================-->
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">

    <!--WRAPPER DEL TITULO O CABECERA-->
    <section class="content-header">
        <h1>
            Cambiar contrase&ntilde;a
        </h1>
        <hr class="btn-warning">
    </section>
    <!--WRAPPER DEL CONTENIDO DE LA PAGINA-->
    <section class="content">

        <form id="form-password" class="form-label-top">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="box box-warning">
                        <div class="box-body">
                            <input type="hidden" name="action" value="changepass"/>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Contrase&ntilde;a actual</label>
                                <input type="password" name="password" id="password" value="" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Contrase&ntilde;a nueva</label>
                                <input type="password" name="password-new" id="password-new" value=""
                                       class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Repetir contrase&ntilde;a</label>
                                <input type="password" name="password-check" id="password-check" value=""
                                       class="form-control"/>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input class="submit btn btn-warning pull-right" id="passsubmit" type="submit" value="Guardar"/>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </section>
</div>
