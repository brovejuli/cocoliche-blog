<?php
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
$dbManager = new DatabaseManager();
?>
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-custom box-solid">
                    <div class="box-header">
                        <h3><i class="fa fa-list"></i> <?= $_GET["title"] ?>
                            <span id="signinButton" class="pre-sign-in pull-right" style="margin-top: -10px">
                              <!-- IMPORTANT: Replace the value of the <code>data-clientid</code>
                                   attribute in the following tag with your project's client ID. -->
                              <span
                                  class="g-signin"
                                  data-callback="signinCallback"
                                  data-clientid="192245719172-9n9i3dpspv5k835shtgtssa2fqp89f5h.apps.googleusercontent.com"
                                  data-cookiepolicy="single_host_origin"
                                  data-scope="https://www.googleapis.com/auth/youtube.upload https://www.googleapis.com/auth/youtube">
                              </span>
                            </span>
                            <img width="50" class="img-responsive img-circle pull-right" id="channel-thumbnail"
                                 style="margin-top: -15px">
                        </h3>
                    </div>
                    <div class="box-body pre-sign-in"><h3 class="title">Iniciar sesion para subir un video.</h3></div>
                    <form id="frm" class="post-sign-in" style="visibility: hidden;">
                        <input type="hidden" name="action" id="action" value="videos">
                        <div class="box-body">
                            <div class="videos"></div>
                            <div class="post-sign-in clearfix">
                                <div class="pull-right text-center">
                                    <!--                                    <span id="channel-name"></span>-->
                                </div>
                            </div>
                            <!--                            <hr>-->
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="fecha">Fecha</label>
                                        <input type="text" name="fecha" id="fecha" class="form-control"
                                               required value="<?= date('d/m/Y') ?>">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="obra">Obra</label>
                                        <select name="obra" id="obra" class="form-control" required>
                                            <option value="">Seleccionar</option>
                                            <?php foreach ($dbManager->select(TABLE_OBRAS, 'nombre', 'ASC') as $obra): ?>
                                                <option
                                                    value="<?= $obra['Id'] ?>"><?= utf8_encode($obra['nombre']) ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="categorias">Categorias</label>
                                        <select name="categorias[]" id="categorias" class="form-control"
                                                multiple="multiple"
                                                data-placeholder="Seleccionar una o varias categorias" required>
                                            <?php foreach ($dbManager->select(TABLE_CATEGORIAS, 'categoria', 'ASC') as $obra): ?>
                                                <option
                                                    value="<?= utf8_encode($obra['categoria']) ?>"><?= utf8_encode($obra['categoria']) ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Videos subidos:</label>
                                        <div class="videos-links"></div>
                                    </div>
                                    <hr>
                                    <br>
                                    <br>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title">T&iacute;tulo</label>
                                            <input id="title" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="privacy-status">Privacidad</label>
                                            <select id="privacy-status" class="form-control">
                                                <option value="public">P&uacute;blico</option>
                                                <option value="unlisted">Oculto</option>
                                                <option value="private">Privado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="description">Descripci&oacute;n</label>
                                        <textarea id="description" class="form-control" cols="10"
                                                  rows="6"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <input type="file" id="file" accept="video/*">
                                        </div>
                                        <div class="col-md-4">
                                            <button id="button" class="btn btn-custom btn-block"><span
                                                    class="fa fa-upload"></span> Subir
                                            </button>
                                        </div>
                                        <div class="during-upload col-md-12" style="margin-top: 40px">
                                            <div class="progress active">
                                                <div
                                                    class="upload-progress progress-bar progress-bar-success progress-bar-striped"
                                                    role="progressbar"
                                                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="1"
                                                    style="width: 0%">
                                                    <span class="sr-only">40% Complete (success)</span>
                                                </div>
                                            </div>
                                            <p><span id="percent-transferred"></span>% completado</p>
                                        </div>
                                        <div class="post-upload col-md-12">
                                            <!--                                            <p>Id del video: <span id="video-id"></span></p>-->
                                            <!--                                            <p>Procesando...</p>-->
                                            <ul id="post-upload-status"></ul>
                                            <div class="box hidden box-loading">
                                                <div class="box-header">
                                                    <h3 class="box-title">Procesando</h3>
                                                </div>
                                                <div class="box-body">
                                                    Por favor aguarde un momento. Youtube est&aacute; procesando su
                                                    solicitud...
                                                </div>
                                                <!-- /.box-body -->
                                                <!-- Loading (remove the following to stop the loading)-->
                                                <div class="overlay">
                                                    <i class="fa fa-refresh fa-spin"></i>
                                                </div>
                                                <!-- end loading -->
                                            </div>
                                            <!-- /.box -->
                                            <!--                                            <div id="player"></div>-->
                                        </div>
                                    </div>
                                    <!--                                <p id="disclaimer">By uploading a video, you certify that you own all rights to the-->
                                    <!--                                    content or that you are-->
                                    <!--                                    authorized by the owner to make the content publicly available on YouTube, and that-->
                                    <!--                                    it otherwise complies-->
                                    <!--                                    with the YouTube Terms of Service located at <a-->
                                    <!--                                        href="http://www.youtube.com/t/terms" target="_blank">http://www.youtube.com/t/terms</a>-->
                                    <!--                                </p>-->
                                </div>
                            </div>
                        </div>
                        <div class="box-footer clearfix">
                            <div class="col-md-12">
                                <div class="col-md-2 pull-right">
                                    <button type="submit" class="btn btn-lg btn-flat btn-custom btn-block btn-save">Guardar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
</section>