<?php
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
$dbManager = new DatabaseManager();
?>
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">

            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3 class="box-title">
                                <h1><i class="fa fa-list"></i> <?= $_GET["title"] ?></h1>
                            </h3>
                        </div>
                        <form id="frm">
                            <input type="hidden" name="action" id="action" value="fotos">

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="fecha">Fecha</label>
                                                <input type="text" name="fecha" id="fecha" class="form-control"
                                                       required value="<?= date('d/m/Y') ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="obra">Obra</label>
                                                <select name="obra" id="obra" class="form-control" required>
                                                    <option value="">Seleccionar</option>
                                                    <?php foreach ($dbManager->select(TABLE_OBRAS, 'nombre', 'ASC') as $obra): ?>
                                                        <option
                                                            value="<?= $obra['Id'] ?>"><?= utf8_encode($obra['nombre']) ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="categorias">Categorias</label>
                                                <select name="categorias[]" id="categorias" class="form-control"
                                                        multiple="multiple"
                                                        data-placeholder="Seleccionar una o varias categorias" required>
                                                    <?php foreach ($dbManager->select(TABLE_CATEGORIAS, 'categoria', 'ASC') as $obra): ?>
                                                        <option
                                                            value="<?= utf8_encode($obra['categoria']) ?>"><?= utf8_encode($obra['categoria']) ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="imagenes">Im&aacute;genes</label>
                                                <input type="file" name="imagen" id="imagen" class="hidden">
                                                <div class="clearfix"></div>
                                                <button type="button" class="btn btn-custom btn-flat" id="file-trigger">
                                                    Seleccionar archivo
                                                </button>
                                                <button type="button" class="btn btn-custom btn-flat" id="subir">Subir
                                                </button>
                                                <br>
                                                <br>
                                                <div class="links"></div>
                                                <div class="files"></div>
                                                <!--<div class="bdr_upload">
                                                    <div id="dropzone" class="dropzone text-center">
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <span class="fa fa-upload fa-5x"></span>
                                                        <p><strong>Arrastrar</strong> o hacer click para a&ntilde;adir
                                                            im&aacute;genes</p>
                                                        <div class="dz-default dz-message">
                                                            <span>Drop files here to upload</span>
                                                        </div>
                                                    </div>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <div class="col-md-12">
                                    <div class="col-md-2 pull-right">
                                        <button type="submit" class="btn btn-lg btn-flat btn-custom btn-block">Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>

            </div>
            <form id="frm" method="post">
                <input type="hidden" name="action" id="action" value="obra"/>
                <input type="hidden" name="Id" id="Id"/>
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Nombre</label>
                                <input type="text" id="nombre" name="nombre" class="form-control clearval"
                                       placeholder="Nombre" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Porcentaje</label>
                                <input type="text" id="porcentaje" name="porcentaje" class="form-control clearval"
                                       placeholder="Porcentaje" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Lugar</label>
                                <input type="text" id="lugar" name="lugar" class="form-control clearval"
                                       placeholder="Lugar" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Email</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select type="text" id="email" name="email[]"
                                                class="form-control email clearval"
                                                placeholder="Email" multiple="multiple" style="width: 100%;">
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="alert alert-danger alert_form" role="alert"></div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Password-->

<!-- Modal Delete !-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <p>¿Seguro que desea eliminar la categoria?</p>
                <p class="debug-url"></p>
                <form id="frmDelete" method="post">
                    <input type="hidden" name="action" value="obra">
                    <input type="hidden" name="Id" id="Id" value="">
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger btn-ok" onclick="doRemove()">Aceptar</a>
            </div>
        </div>
    </div>
</div>