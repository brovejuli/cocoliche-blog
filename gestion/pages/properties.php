<!--==============================CONTENT=================================-->
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="container-fluid">
            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3 class="box-title">
                                <h1><i class="fa fa-home"></i> <?= $_GET["title"] ?></h1>
                            </h3>
                        </div>
                        <div class="box-body table-responsive table-warning">
                            <div id="toolbar" class="btn-group">
                                <button type="button" class="btn btn-default" onclick="doNew()"
                                        data-toggle="modal" data-target="#modal_form">
                                    <i class="glyphicon glyphicon-plus"></i> Nuevo
                                </button>
                            </div>
                            <table class="table table-hover table-striped data-table"
                                   data-toggle="table"
                                   data-url="listar.php?action=properties"
                                   data-query-params="queryParams"
                                   data-pagination="true"
                                   data-search="true"
                                   data-height="600"
                                   data-click-to-select="true"
                                   data-show-refresh="true"
                                   data-show-toggle="true"
                                   data-show-columns="true"
                                   data-toolbar="#toolbar">
                                <thead>
                                <tr>
                                    <th data-field="Id" data-sortable="true">Codigo</th>
                                    <th data-field="price" data-sortable="true">Precio</th>
                                    <th data-field="consult" data-sortable="true">Consultar</th>
                                    <th data-field="state" data-sortable="true">Estado</th>
                                    <th data-field="type" data-sortable="true">Tipo</th>
                                    <th data-field="province" data-sortable="true">Provincia</th>
                                    <th data-field="action" data-formatter="actionFormatter" data-events="actionEvents" data-width="15%" data-class="text-center">
                                        Acciones
                                    </th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                        <div class="box-footer clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal Delete !-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <form id="frmDelete" method="post">
                    <input type="hidden" name="action" value="property">
                    <input type="hidden" name="Id" id="Id" value="">
                </form>
                <p>Esta seguro que desea eliminar definitivamente el post?</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok" onclick="doRemove()">Delete</a>
            </div>
        </div>
    </div>
</div>