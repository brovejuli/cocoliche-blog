<div class="modal fade modal-success" id="create-trip-success" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">EXITO!</h4>
            </div>
            <div class="modal-body">
                <p>Propiedad cargada con exito!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom btn-flat" onClick="window.location.href = 'main.php?page=properties&title=Venta/Alquiler';">Aceptar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->4
</div><!-- /.modal --

<div class="modal fade modal-success" id="create-activity-success" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">EXITO!</h4>
            </div>
            <div class="modal-body">
                <p>Actividad creada exito!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom btn-flat" onClick="window.location.href = 'main.php?page=activities&title=Actividades';">Aceptar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade modal-success" id="create-new-success" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">EXITO!</h4>
            </div>
            <div class="modal-body">
                <p>Noticia creada exito!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom btn-flat" onClick="window.location.href = 'main.php?page=news&title=Noticias';">Aceptar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->