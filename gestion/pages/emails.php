<!--==============================CONTENT=================================-->
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">
            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3 class="box-title">
                                <h1><i class="fa fa-envelope"></i> <?= $_GET["title"] ?></h1>
                            </h3>
                        </div>
                        <div class="box-body table-responsive table-warning">
                            <table class="table table-hover table-striped data-table"
                                   data-toggle="table"
                                   data-url="listar.php?action=emails"
                                   data-query-params="queryParams"
                                   data-pagination="true"
                                   data-search="true"
                                   data-height="600"
                                   data-click-to-select="true"
                                   data-show-refresh="true"
                                   data-show-toggle="true"
                                   data-show-columns="true"
                                   data-toolbar="#toolbar"
                                   data-show-export="true"
                                   data-export-types="['excel', 'pdf']"
                                   data-export-options='{
                                     "fileName": "Emails",
                                     "worksheetName": "list",
                                     "jspdf": {
                                       "autotable": {
                                         "styles": { "rowHeight": 20, "fontSize": 10 },
                                         "headerStyles": { "fillColor": 255, "textColor": 0 },
                                         "alternateRowStyles": { "fillColor": [60, 69, 79], "textColor": 255 },
                                       }
                                     }
                                   }'>
                                <thead>
                                <tr>
                                    <th data-field="name" data-sortable="true">Nombre</th>
                                    <th data-field="email" data-sortable="true">Email</th>
                                    <th data-field="subject" data-sortable="true">Consulta</th>
                                    <th data-field="message" data-sortable="true">Cometario</th>
                                    <th data-field="source" data-sortable="true">Fuente</th>
                                    <th data-field="date" data-sortable="true">Fecha de solicitud</th>
                                    <th data-field="action" data-formatter="actionFormatter" data-events="actionEvents"
                                        class="col-md-2 text-center">
                                        Acciones
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="box-footer clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal Delete !-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <p>¿Esta seguro que desea eliminar la suscripcion?.</p>
                <form id="frmDelete" method="post">
                    <input type="hidden" name="action" value="email">
                    <input type="hidden" name="Id" id="Id" value="">
                </form>
                <p class="debug-url"></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger btn-ok" onclick="doRemove()">Borrar</a>
            </div>
        </div>
    </div>
</div>