<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row text-center">
            <h1><span class="fa fa-thumbs-o-down"></span></h1>
            <h2 class="font-bold">P&aacute;gina no encontrada</h2>

            <div class="error-desc">
                Lo sentimos, la pagina que solicit&oacute; no existe.
            </div>
        </div>
    </section>
</div>
