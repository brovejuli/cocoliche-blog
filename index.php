<?php

include_once 'includes/configure.php';
include_once 'gestion/class/DatabaseManagerFrontEnd.class.php';
include_once 'gestion/class/Configuration.class.php';

$dbManager = new DatabaseManagerFrontEnd();
$configuration = new Configuration();
$configuration = Main::query_converter($configuration->getConfigurationValues());
?>

<!DOCTYPE HTML>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>COCOLICHE Ropa con Oportunidad</title>
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css'/>
    <link href="css/font-awesome.min.css" rel='stylesheet' type='text/css'/>

    <link rel="shortcut icon" href="images/favicon.ico"/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!---- start-smoth-scrolling---->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- Custom Theme files -->
    <link href="css/style.css" rel='stylesheet' type='text/css'/>
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
    <script type="text/javascript" src="js/modal.js"></script>
    <script type="text/javascript" src="js/send_mail.js"></script>
    <script>
        $(function () {
            var pull = $('#pull');
            menu = $('nav ul');
            menuHeight = menu.height();
            $(pull).on('click', function (e) {
                e.preventDefault();
                menu.slideToggle();
            });
            $(window).resize(function () {
                var w = $(window).width();
                if (w > 320 && menu.is(':hidden')) {
                    menu.removeAttr('style');
                }
            });
        });
    </script>
    <!----//End-top-nav-script---->
</head>
<body>
<!-----start-container----->
<!-----header-section------>
<div class="header-section">
    <!----- start-header---->
    <div id="home" class="header fixed">
        <div class="container">
            <div class="pull-right direccion">
                <?= $configuration['Empresa_Direccion'] ?>
            </div>
        </div>
        <div class="top-header">
            <div class="container">
                <div class="logo animated owl-fadeUp-in">
                    <a href="#"><img style="width: 50%" src="images/logo-image.png" title="logo"/></a>
                </div>
                <!----start-top-nav---->
                <nav class="top-nav">
                    <ul class="top-nav">
                        <li class="page-scroll"><a href="#gallery" class="scroll">COMPRAR</a></li>

                        <li class="page-scroll"><a href="#fea" class="scroll">VENDER</a></li>
                        <li class="page-scroll"><a href="#mini" class="scroll">MINICOCOLICHE</a></li>

                        <li class="page-scroll"><a href="#" class="scroll" data-toggle="modal"
                                                   data-target=".bs-example-modal-lg">FAQs </a></li>
                        <!--<li class="contatct-active" class="page-scroll"><a href="#blog" class="scroll">BLOG</a></li>-->

                        <li class="contatct-active" class="page-scroll"><a href="#contact" class="scroll">CONTACTO</a>
                        </li>

                        <li class="page-scroll"><a href="#" class="scroll tucuenta">TU CUENTA</a></li>
                        <li class="redes"><a href="<?= $configuration['Url_Facebook'] ?>"
                                             target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="redes"><a href="<?= $configuration['Url_Instagram'] ?>"
                                             target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>


                    </ul>
                    <a href="#" id="pull"><img src="images/nav-icon.png" title="menu"/></a>
                </nav>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
    <!----- //End-header---->

    <!----EMPIEZA MODAL DE FAQS---->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>

                <!--FAQS-->
                <div id="faqs">

                    <div class="container">
                        <div class="comprar">
                            <h1>PREGUNTAS FRECUENTES</h1>
                        </div>
                        <div class="col-md-8 features-info">
                            <h3>COMPRAR</h3>

                            <ul>
                                <li><h4>¿Cuáles son los medios de Pago?</h4>
                                    <p>Se puede abonar en efectivo o con débito y crédito de VISA (15% de recargo hasta
                                        3 cuotas)</p>

                                <li><h4>¿Cada cuánto reponen la ropa?</h4>
                                <li>
                                    <p>La ropa se renueva constantemente durante todo el día</p>

                                <li>
                                    <h4>¿Tengo que traer ropa para poder comprar?</h4>
                                    <p>No, se puede comprar en la tienda directamente sin necesidad de traer ropa.</p>
                                </li>
                                <li>
                                    <h4>¿Las prendas tienen cambio?</h4>
                                    <p>Las prendas no tienen cambio ni devoluciones.</p>
                                </li>

                                <li>
                                    <h4>¿En qué me llevo la ropa que compro?</h4>
                                    <p>Podés traer tu propia bolsa o llevarte un reutilizable de Cocoliche por $10</p>
                                </li>
                                <li>
                                    <h4>¿Me puedo probar lo que quiero?</h4>
                                    <p>Sí, hasta 5 prendas por vez.</p>
                                </li>
                                <li>
                                    <h4>¿Se pueden hacer reservas?</h4>
                                    <p>No tomamos reservas</p>
                                </li>
                            </ul>

                            <hr>
                            <h3>VENDER</h3>

                            <ul>
                                <li><h4>¿Cómo funciona Cocoliche?</h4>
                                    <p>En Cocoliche seleccionamos las prendas, y las tomamos en
                                        <span>consignación. </span> A medida que la ropa se vende se obtiene un crédito
                                        del <span>50% </span>del precio, para prendas de marca reconocidas de shopping
                                        (listado disponible en mostrador de selección); y un crédito del
                                        <span>33%</span> para el resto de las prendas. Para ropa de niño, obtenés un
                                        <span>40%</span> del valor y tomamos principalmente de las marcas más conocidas.
                                        Este dinero se puede usar en ropa o retirar en efectivo.</p>

                                <li><h4>¿Quién pone los precios?</h4>
                                <li>
                                    <p>Los precios se determinan en Cocoliche en el momento de la selección, en función
                                        de otras prendas similares que estén a la venta. Se toman en consideración
                                        factores como el estado, la marca, el tipo de prenda y la calidad.</p>

                                <li>
                                    <h4>¿Qué tipo de ropa aceptan?</h4>
                                    <p>Aceptamos ropa que esté en perfectas condiciones y lista para la venta. De varón,
                                        mujer y niños.
                                        , lavadas y planchadas. Se aceptan también calzado sin uso (excepto de niños) y
                                        carteras.
                                        En Cocoliche decidimos qué ropa se acepta y qué no, en función de lo que
                                        consideramos que es vendible en la tienda.</p>
                                </li>
                                <li>
                                    <h4>¿Cuándo puedo traer mi ropa?</h4>
                                    <p>Recibimos la ropa los mismos días en que la tienda está abierta y se selecciona
                                        en el momento.</p>
                                </li>

                                <li>
                                    <h4>¿Hay una cantidad máxima o mínima de prendas para traer?</h4>
                                    <p>No, se puede traer toda la ropa que se quiera.</p>
                                </li>
                                <li>
                                    <h4>¿Cómo me entero de mi crédito?</h4>
                                    <p>Todos los meses, a principio de mes, se informa vía mail el crédito disponible.
                                        También se puede consultar personalmente en la tienda o via mail en cualquier
                                        momento.</p>
                                </li>
                                <li>
                                    <h4>¿Qué pasa con la ropa que no se vende?</h4>
                                    <p>En Cocoliche se seleccionan prendas que son de fácil venta en la tienda. Así, por
                                        lo general las prendas se venden dentro de su temporada. En el caso que esto no
                                        ocurra, las prendas pueden ser guardadas hasta la próxima temporada o retiradas
                                        por sus dueños.</p>
                                </li>
                                <li>
                                    <h4>¿Reciben ropa que no sea de marca?</h4>
                                    <p>Tomamos ropa de marca o no. Las prendas de las marcas que valoramos las tomamos
                                        al <span>50%</span> y el resto de las prendas al <span>33%.</span></p>
                                </li>
                                <li>
                                    <h4>¿Cuáles son las marcas que valoramos al <span>50%</span>?</h4>

                                    <p class="col-md-4 marcas"> 47street <br>
                                        <span>A</span> <br>
                                        A +, Amores trash couture, Anima, Ay not Dead
                                        Abercrombie & Fitch AbsolutJoy, Adidas, Adolfo Dominguez
                                        Adriana Constantini, Akiabara, AlloMartinez, Anushka Elliot
                                        Armany, Awada, Ayres <br>
                                        <span>B</span> <br>
                                        Banana Republic, Belen Amigo, Bendito Pie,
                                        Benetton, Benito Fernandez, Bensimon, Bershka
                                        Billabong, Blackmamba, Blaquè, Bolivia, Bowen, Burberry <br>
                                        <span>C</span> <br>
                                        Cacharel, Calvin KleiN, Cardon, Cher, Chinita
                                        Chocolate, Christian Lacroix, Cibeles, Clara Ibarguren
                                        Columbia, Como quieres que te quiera, Complot, Converse
                                        Cook, Cora Groppo <br>
                                        <span>D</span> <br>
                                        DC, De la Ostia, Deleon, Desiderata, Desigual,
                                        Diesel, Divided (H&M), Dior, Dolce Gabana,
                                        Doma Worldwide, Dubié <br>
                                        <span>E</span> <br>
                                        El Burgues, Etiqueta Negra, Evangelina Bomparola <br>
                                        <span>F</span> <br>
                                        Felix, Ferraro, Forever 21
                                        <span>G</span> <br>
                                    </p>
                                    <p class="col-md-4 marcas">

                                        Gap, Garçon Garcia, Giesso, Ginebra, Gola
                                        Gucci, Guess <br>
                                        <span>H</span> <br>
                                        H&M, Herencia Argentina, Hunter, Hush Puppies <br>
                                        <span>I</span> <br>
                                        India Style <br>
                                        <span>J</span> <br>
                                        Jackie Smith, Jazmin Chebar, John L Cook
                                        Juana de arco, Juanita Jo, Julieta Grana <br>
                                        <span>K</span> <br>
                                        Kevingston, Key Biscayne, Kill, Kosiuko,
                                        Kostume <br>
                                        <span>L</span> <br>
                                        Lacoste, La Dolfina, La Martina, Las Oreiro,
                                        Las Pepas, Lazaro, Lee, Levis, Lupe <br>
                                        <span>M</span> <br>
                                        Made in Chola, Maggio Rosseto, Mango, Maria Cher
                                        Maria Vazquez, Mariana Dappiano, Markova, Max Mara,
                                        Mishka, Miss Sixty, Montagne, Muaa <br>
                                        <span>N</span> <br>
                                        Naima, NAM, Natalia Antolin, Nike, Nous Etudios <br>
                                        <span>O</span> <br>
                                        Ona Saez, Ossira <br>


                                    </p>
                                    <p class="col-md-4 marcas">

                                        <span>P</span> <br>
                                        Pablo Ramírez, Paris, Paruolo
                                        Paula Cahen danvers, Paula y Agustina Ricci, Penguin
                                        Pepe Canterol, Pepe Jeans, Perramus, Persaman N.Y.
                                        Pink (de Victoria Secret), Polo Ralph Laurent,
                                        Pony, Portsaid, Posse, Prune, Pull & Bear, Puma <br>
                                        <span>Q</span> <br>
                                        Quicksilver <br>
                                        <span>R</span> <br>
                                        Ralph Laurent, Rapsodia, Ray Ban, Ricky Sarkany,
                                        Rie, RipCurl <br>
                                        <span>S</span> <br>
                                        Sofia Sarkany, System Basic <br>
                                        <span>T</span> <br>
                                        Talitha Ind, Tascani, Them, The North Face
                                        Timberland, Tommy, Tramando, Trosman, Tucci <br>
                                        <span>U</span> <br>
                                        Uma, U.S. Polo Assn. <br>
                                        <span>V</span> <br>
                                        Vans, Ver, Vero Alfie, Versace, Vevu, Vitamina <br>
                                        <span>W</span> <br>
                                        Wanama, Wrangler <br>
                                        <span>Y</span> <br>
                                        Yagmour, Yves Saint Laurent <br>
                                        <span>Z</span> <br>
                                        Zara <br>


                                    </p>

                                </li>


                                <li class="col-md-12">
                                    <h4>¿Cuáles son las marcas de niños que tomamos principalmente?</h4>
                                    <p>47 Street Kids, Adidas Kids, Atomik, Baby Cottons, Banana Republic Kids,
                                        Benetton, Carter’s, Cheeky, Coniglio, Crocs Kids, Disney, Gap Kids, Grisino,
                                        Gymboree, H&M Kids, Hello Kitty, Kevingston, Kosiuko, La Jolie, Levi’s, Little
                                        Akiabara, Magdalena Esposito, Mimo & Co., Mini Complot, Montagne, Nike Kids, Old
                                        Navy Kids, Ona Saez Kids, Oshkosh, Owoko, Paula Cahen D’Anvers Kids, Pecosos,
                                        Pioppa, Polo Kids, Prénatal, Primera Huella, Ralph Lauren, Rapsodia Kids, Tommy
                                        Hilfiger Kids, Topper Kids, Vans Kids, Zara Kid</p>
                                </li>

                            </ul>

                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
    <!----TERMINA MODAL de faqs--->

    <!----EMPIEZA MODAL DE FAQS-COMPRAR---->
    <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>

                <!--FAQS-->
                <div id="faqs-comprar">

                    <div class="container">
                        <div class="comprar">
                            <h1>PREGUNTAS FRECUENTES</h1>
                        </div>
                        <div class="col-md-8 features-info">
                            <h3>COMPRAR</h3>

                            <ul>
                                <li><h4>¿Cuáles son los medios de Pago?</h4>
                                    <p>Se puede abonar en efectivo o con débito y crédito de VISA (15% de recargo hasta
                                        3 cuotas)</p>

                                <li><h4>¿Cada cuánto reponen la ropa?</h4>
                                <li>
                                    <p>La ropa se renueva constantemente durante todo el día</p>

                                <li>
                                    <h4>¿Tengo que traer ropa para poder comprar?</h4>
                                    <p>No, se puede comprar en la tienda directamente sin necesidad de traer ropa.</p>
                                </li>
                                <li>
                                    <h4>¿Las prendas tienen cambio?</h4>
                                    <p>Las prendas no tienen cambio ni devoluciones.</p>
                                </li>

                                <li>
                                    <h4>¿En qué me llevo la ropa que compro?</h4>
                                    <p>Podés traer tu propia bolsa o llevarte un reutilizable de Cocoliche por $10</p>
                                </li>
                                <li>
                                    <h4>¿Me puedo probar lo que quiero?</h4>
                                    <p>Sí, hasta 5 prendas por vez.</p>
                                </li>
                                <li>
                                    <h4>¿Se pueden hacer reservas?</h4>
                                    <p>No tomamos reservas</p>
                                </li>
                            </ul>


                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
    <!----TERMINA MODAL de faqs-comprar--->

    <!----EMPIEZA MODAL DE FAQS-vender---->
    <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>

                <!--FAQS-->
                <div id="faqs-comprar">

                    <div class="container">
                        <div class="comprar">
                            <h1>PREGUNTAS FRECUENTES</h1>
                        </div>
                        <div class="col-md-8 features-info">
                            <h3>VENDER</h3>

                            <ul>
                                <li><h4>¿Cómo funciona Cocoliche?</h4>
                                    <p>En Cocoliche seleccionamos las prendas, y las tomamos en
                                        <span>consignación.</span> A medida que la ropa se vende se obtiene un crédito
                                        del <span>50%</span> del precio, para prendas de marca reconocidas de shopping
                                        (listado disponible en mostrador de selección); y un crédito del
                                        <span>33%</span> para el resto de las prendas. Para ropa de niño, obtenés un
                                        <span>40%</span> del valor y tomamos principalmente de las marcas más conocidas.
                                        Este dinero se puede usar en ropa o retirar en efectivo.</p>

                                <li><h4>¿Quién pone los precios?</h4>
                                <li>
                                    <p>Los precios se determinan en Cocoliche en el momento de la selección, en función
                                        de otras prendas similares que estén a la venta. Se toman en consideración
                                        factores como el estado, la marca, el tipo de prenda y la calidad.</p>

                                <li>
                                    <h4>¿Qué tipo de ropa aceptan?</h4>
                                    <p>Aceptamos ropa que esté en perfectas condiciones y lista para la venta. De varón,
                                        mujer y niños.
                                        , lavadas y planchadas. Se aceptan también calzado sin uso (excepto de niños) y
                                        carteras.
                                        En Cocoliche decidimos qué ropa se acepta y qué no, en función de lo que
                                        consideramos que es vendible en la tienda.</p>
                                </li>
                                <li>
                                    <h4>¿Cuándo puedo traer mi ropa?</h4>
                                    <p>Recibimos la ropa los mismos días en que la tienda está abierta y se selecciona
                                        en el momento.</p>
                                </li>

                                <li>
                                    <h4>¿Hay una cantidad máxima o mínima de prendas para traer?</h4>
                                    <p>No, se puede traer toda la ropa que se quiera.</p>
                                </li>
                                <li>
                                    <h4>¿Cómo me entero de mi crédito?</h4>
                                    <p>Todos los meses, a principio de mes, se informa vía mail el crédito disponible.
                                        También se puede consultar personalmente en la tienda o via mail en cualquier
                                        momento.</p>
                                </li>
                                <li>
                                    <h4>¿Qué pasa con la ropa que no se vende?</h4>
                                    <p>En Cocoliche se seleccionan prendas que son de fácil venta en la tienda. Así, por
                                        lo general las prendas se venden dentro de su temporada. En el caso que esto no
                                        ocurra, las prendas pueden ser guardadas hasta la próxima temporada o retiradas
                                        por sus dueños.</p>
                                </li>
                                <li>
                                    <h4>¿Reciben ropa que no sea de marca?</h4>
                                    <p>Tomamos ropa de marca o no. Las prendas de las marcas que valoramos las tomamos
                                        al <span>50%</span> y el resto de las prendas al <span>33%.</span></p>
                                </li>
                                <li>
                                    <h4>¿Cuáles son las marcas que valoramos al <span>50%</span>?</h4>

                                    <p class="col-md-4 marcas"> 47street <br>
                                        <span>A</span> <br>
                                        A +, Amores trash couture, Anima, Ay not Dead
                                        Abercrombie & Fitch AbsolutJoy, Adidas, Adolfo Dominguez
                                        Adriana Constantini, Akiabara, AlloMartinez, Anushka Elliot
                                        Armany, Awada, Ayres <br>
                                        <span>B</span> <br>
                                        Banana Republic, Belen Amigo, Bendito Pie,
                                        Benetton, Benito Fernandez, Bensimon, Bershka
                                        Billabong, Blackmamba, Blaquè, Bolivia, Bowen, Burberry <br>
                                        <span>C</span> <br>
                                        Cacharel, Calvin KleiN, Cardon, Cher, Chinita
                                        Chocolate, Christian Lacroix, Cibeles, Clara Ibarguren
                                        Columbia, Como quieres que te quiera, Complot, Converse
                                        Cook, Cora Groppo <br>
                                        <span>D</span> <br>
                                        DC, De la Ostia, Deleon, Desiderata, Desigual,
                                        Diesel, Divided (H&M), Dior, Dolce Gabana,
                                        Doma Worldwide, Dubié <br>
                                        <span>E</span> <br>
                                        El Burgues, Etiqueta Negra, Evangelina Bomparola <br>
                                        <span>F</span> <br>
                                        Felix, Ferraro, Forever 21
                                        <span>G</span> <br>
                                    </p>
                                    <p class="col-md-4 marcas">

                                        Gap, Garçon Garcia, Giesso, Ginebra, Gola
                                        Gucci, Guess <br>
                                        <span>H</span> <br>
                                        H&M, Herencia Argentina, Hunter, Hush Puppies <br>
                                        <span>I</span> <br>
                                        India Style <br>
                                        <span>J</span> <br>
                                        Jackie Smith, Jazmin Chebar, John L Cook
                                        Juana de arco, Juanita Jo, Julieta Grana <br>
                                        <span>K</span> <br>
                                        Kevingston, Key Biscayne, Kill, Kosiuko,
                                        Kostume <br>
                                        <span>L</span> <br>
                                        Lacoste, La Dolfina, La Martina, Las Oreiro,
                                        Las Pepas, Lazaro, Lee, Levis, Lupe <br>
                                        <span>M</span> <br>
                                        Made in Chola, Maggio Rosseto, Mango, Maria Cher
                                        Maria Vazquez, Mariana Dappiano, Markova, Max Mara,
                                        Mishka, Miss Sixty, Montagne, Muaa <br>
                                        <span>N</span> <br>
                                        Naima, NAM, Natalia Antolin, Nike, Nous Etudios <br>
                                        <span>O</span> <br>
                                        Ona Saez, Ossira <br>


                                    </p>
                                    <p class="col-md-4 marcas">

                                        <span>P</span> <br>
                                        Pablo Ramírez, Paris, Paruolo
                                        Paula Cahen danvers, Paula y Agustina Ricci, Penguin
                                        Pepe Canterol, Pepe Jeans, Perramus, Persaman N.Y.
                                        Pink (de Victoria Secret), Polo Ralph Laurent,
                                        Pony, Portsaid, Posse, Prune, Pull & Bear, Puma <br>
                                        <span>Q</span> <br>
                                        Quicksilver <br>
                                        <span>R</span> <br>
                                        Ralph Laurent, Rapsodia, Ray Ban, Ricky Sarkany,
                                        Rie, RipCurl <br>
                                        <span>S</span> <br>
                                        Sofia Sarkany, System Basic <br>
                                        <span>T</span> <br>
                                        Talitha Ind, Tascani, Them, The North Face
                                        Timberland, Tommy, Tramando, Trosman, Tucci <br>
                                        <span>U</span> <br>
                                        Uma, U.S. Polo Assn. <br>
                                        <span>V</span> <br>
                                        Vans, Ver, Vero Alfie, Versace, Vevu, Vitamina <br>
                                        <span>W</span> <br>
                                        Wanama, Wrangler <br>
                                        <span>Y</span> <br>
                                        Yagmour, Yves Saint Laurent <br>
                                        <span>Z</span> <br>
                                        Zara <br>


                                    </p>

                                </li>
                            </ul>


                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
    <!----TERMINA MODAL de faqs-vender--->

    <!----EMPIEZA MODAL DE FAQS-vender-mini---->
    <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>

                <!--FAQS-->
                <div id="faqs-comprar">

                    <div class="container">
                        <div class="comprar">
                            <h1>PREGUNTAS FRECUENTES</h1>
                        </div>
                        <div class="col-md-8 features-info">
                            <h3>VENDER</h3>

                            <ul>
                                <li><h4>¿Cómo funciona Cocoliche?</h4>
                                    <p>En Cocoliche seleccionamos las prendas, y las tomamos en
                                        <span>consignación.</span> A medida que la ropa se vende se obtiene un crédito
                                        del <span>50%</span> del precio, para prendas de marca reconocidas de shopping
                                        (listado disponible en mostrador de selección); y un crédito del
                                        <span>33%</span> para el resto de las prendas. Para ropa de niño, obtenés un
                                        <span>40%</span> del valor y tomamos principalmente de las marcas más conocidas.
                                        Este dinero se puede usar en ropa o retirar en efectivo.</p>

                                <li><h4>¿Quién pone los precios?</h4>
                                <li>
                                    <p>Los precios se determinan en Cocoliche en el momento de la selección, en función
                                        de otras prendas similares que estén a la venta. Se toman en consideración
                                        factores como el estado, la marca, el tipo de prenda y la calidad.</p>

                                <li>
                                    <h4>¿Qué tipo de ropa aceptan?</h4>
                                    <p>Aceptamos ropa que esté en perfectas condiciones y lista para la venta. De varón,
                                        mujer y niños.
                                        , lavadas y planchadas. Se aceptan también calzado sin uso (excepto de niños) y
                                        carteras.
                                        En Cocoliche decidimos qué ropa se acepta y qué no, en función de lo que
                                        consideramos que es vendible en la tienda.</p>
                                </li>
                                <li>
                                    <h4>¿Cuándo puedo traer mi ropa?</h4>
                                    <p>Recibimos la ropa los mismos días en que la tienda está abierta y se selecciona
                                        en el momento.</p>
                                </li>

                                <li>
                                    <h4>¿Hay una cantidad máxima o mínima de prendas para traer?</h4>
                                    <p>No, se puede traer toda la ropa que se quiera.</p>
                                </li>
                                <li>
                                    <h4>¿Cómo me entero de mi crédito?</h4>
                                    <p>Todos los meses, a principio de mes, se informa vía mail el crédito disponible.
                                        También se puede consultar personalmente en la tienda o via mail en cualquier
                                        momento.</p>
                                </li>
                                <li>
                                    <h4>¿Qué pasa con la ropa que no se vende?</h4>
                                    <p>En Cocoliche se seleccionan prendas que son de fácil venta en la tienda. Así, por
                                        lo general las prendas se venden dentro de su temporada. En el caso que esto no
                                        ocurra, las prendas pueden ser guardadas hasta la próxima temporada o retiradas
                                        por sus dueños.</p>
                                </li>
                                <li>
                                    <h4>¿Reciben ropa que no sea de marca?</h4>
                                    <p>Tomamos ropa de marca o no. Las prendas de las marcas que valoramos las tomamos
                                        al <span>50%</span> y el resto de las prendas al <span>33%.</span></p>
                                </li>

                                <li>
                                    <h4>¿Cuáles son las marcas de niños que tomamos principalmente?</h4>
                                    <p>47 Street Kids, Adidas Kids, Atomik, Baby Cottons, Banana Republic Kids,
                                        Benetton, Carter’s, Cheeky, Coniglio, Crocs Kids, Disney, Gap Kids, Grisino,
                                        Gymboree, H&M Kids, Hello Kitty, Kevingston, Kosiuko, La Jolie, Levi’s, Little
                                        Akiabara, Magdalena Esposito, Mimo & Co., Mini Complot, Montagne, Nike Kids, Old
                                        Navy Kids, Ona Saez Kids, Oshkosh, Owoko, Paula Cahen D’Anvers Kids, Pecosos,
                                        Pioppa, Polo Kids, Prénatal, Primera Huella, Ralph Lauren, Rapsodia Kids, Tommy
                                        Hilfiger Kids, Topper Kids, Vans Kids, Zara Kid</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----TERMINA MODAL de faqs-vender--->


    <!----EMPIEZA MODAL DE NOTAS DE BLOG---->
    <div class="modal fade blognotas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <!--NOTA DE BLOG-->
                <div id="faqs">
                    <div class="container">
                        <div class="comprar ">
                            <h1 class="titulo">Sumate a la moda sustentable</h1>
                        </div>
                        <div class="col-md-8 features-info">
                            <div class="col-md-4"><img src="images/valija.jpg" class="img-responsive imagen"></div>
                            <div class="col-md-8">
                                <div class="text-container" style="overflow-y: scroll; height: 50vh">
                                    <h3 class="copete">Como vos, hay muchas personas que se acercan a la tienda con idea
                                        de alivianar su
                                        equipaje y liberar su ropa del placard permitiendo que otros puedan
                                        reutilizarla,
                                        por eso decimos que somos una comunidad de ropa con otra oportunidad.</h3>
                                    <div class="cuerpo">
                                        <p>

                                            En Cocoliche te proponemos soltar esas prendas que te acompañan para que
                                            puedan
                                            llegar a manos de otras personas, sin lugar a dudas esta es una experiencia
                                            gratificante, la sensación es de libertad y liviandad.
                                        </p>

                                        <p> Para Patricia, colaboradora de este espacio, dejar ropa a la venta es una
                                            experiencia superadora ya que “uno se siente reconfortado porque dejás algo
                                            que otro
                                            puede utilizar y vos te sacás una carga negativa”.</p>
                                    </div>
                                </div>
                                <div class="pull-left">
                                    <img src="images/logo-image.png" style="width: 60px">

                                </div>
                                <div class="banda2 text-center">
                                    <div class="container text-center">


                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
    <!----TERMINA MODAL DE BLOG DE NOTAS--->
    <!----start-slider-script---->
    <script src="js/responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: true,
                pager: true,
                nav: true,
                speed: 1,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <!----//End-slider-script---->
    <!-- Slideshow 4 -->

    <div id="top" class="callbacks_container">
        <ul class="rslides" id="slider4">
            <li>
                <img src="images/local.jpg" alt="">
                <div class="caption">

                    <div class=" text-center col-md-12">
                        <h1 style="color: #ffffff !important;"><span style="color:#ffff33 !important">COMPRÁ</span></h1>

                        <div class="">
                            <h1 style="color: #FFFFFF !important; font-family: 'Roboto Condensed'; font-style: italic; font-weight: 700; font-size: 35px;">
                                ROPA CON OTRA OPORTUNIDAD </h1>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <img src="images/local.jpg" alt="">
                <div class="caption">
                    <div class="text-center col-md-12">
                        <h1 style="color: #ffffff !important;"><span style="color:#ffff33 !important">VENDÉ</span></h1>

                        <div class="">
                            <h1 style="color: #FFFFFF !important; font-family: 'Roboto Condensed', sans-serif; font-style: italic; font-weight: bold; font-size: 35px;">
                                ROPA CON OTRA OPORTUNIDAD </h1>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <img src="images/local.jpg" alt="">
                <div class="caption">
                    <div class="text-center col-md-12">
                        <h1 style="color: #ffffff !important;"><span style="color:#ffff33 !important">REUTILIZÁ</span>
                        </h1>

                        <div class="">
                            <h1 style="color: #FFFFFF !important; font-family: 'Roboto Condensed', sans-serif; font-style: italic; font-weight: bold; font-size: 35px;">
                                ROPA CON OTRA OPORTUNIDAD </h1>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="clearfix"></div>
    <!-----divice----->

    <!---//divice----->
    <!----- //End-slider---->
</div>
<!-----//header-section----->
<!----team---->
<div id="about" class="team col-md-offset-2 col-md-8 col-md-offset-2 col-sm-12">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
        <img class="img-responsive text-center" src="images/1.png">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
        <img class="img-responsive text-center" src="images/2.png">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
        <img class="img-responsive text-center" src="images/3.png">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
        <img class="img-responsive text-center" src="images/4.png">
    </div>


</div>
<div class="clearfix"></div>

</div>
</div>


<div class="banda text-center">
    <div class="container">


    </div>
</div>


<!----comprá---->
<div>
    <div class="container">

        <div id="gallery" class="team">

            <div class="team-members">
                <div class="container">
                    <div class="col-md-12 comprar">
                        <h1>COMPRAR</h1>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 team-member">
                        <div class="team-member-info">
                            <img class="member-pic img-responsive" src="images/team-member.jpg" title="name"/>

                            <label class="team-member-caption text-center">
                                <div class="comprar">
                                    <h1 style="color:#ffff33; font-weight: normal; padding-top: 100px"> MUJER</h1>
                                </div>


                            </label>
                        </div>
                    </div><!--- end-team-member --->
                    <div class="col-md-4 col-sm-4 col-xs-12 team-member">
                        <div class="team-member-info">
                            <img class="member-pic img-responsive" src="images/team-member1.jpg" title="name"/>

                            <label class="team-member-caption text-center">
                                <div class="comprar">
                                    <h1 style="color:#ffff33; font-weight: normal; padding-top: 100px"> HOMBRE</h1>
                                </div>
                            </label>
                        </div>
                    </div><!--- end-team-member --->
                    <div class="col-md-4 col-sm-4 col-xs-12 team-member">
                        <div class="team-member-info">
                            <img class="member-pic img-responsive" src="images/team-member2.jpg" title="name"/>

                            <label class="team-member-caption text-center">
                                <div class="comprar">
                                    <h1 style="color:#ffff33; font-weight: normal; padding-top: 100px"> MINI</h1>
                                </div>
                            </label>
                        </div>
                    </div><!--- end-team-member --->
                    <div class="col-md-8 col-xs-12 comprar">
                        <h1 style="font-weight: normal !important; font-weight: 400">Reutilizar en Cocoliche es una
                            buena idea <a href="#" class="scroll" data-toggle="modal"
                                          data-target=".bs-example-modal-lg1"><img src="images/mas.png"></a></h1>
                    </div>
                    <div class="col-md-4 comprar text-right pull-right">
                        <br>
                        <img class="img-responsive" src="images/iconos_reciclado.png">
                        <br><br>

                    </div>

                    <div class="clearfix"></div>
                    <!--//team-members---->
                </div>
            </div>

        </div>
        <!----sreen-gallery-cursual---->
        <div class="sreen-gallery-cursual">
            <!-- requried-jsfiles-for owl -->
            <link href="css/owl.carousel.css" rel="stylesheet">
            <script src="js/owl.carousel.js"></script>
            <script>
                $(document).ready(function () {
                    $("#owl-demo").owlCarousel({
                        items: 3,
                        lazyLoad: true,
                        autoPlay: true,
                    });
                });
            </script>

        </div>
    </div>


    <div class="clearfix"></div>


    <!---- SOMOS ---->
    <div id="somos" class="frase">
        <div class="container">
            <div class="col-md-12 text-center">
                <h1>VENDÉ LO QUE NO USÁS. COMPRÁ PRENDAS ÚNICAS. REUTILIZÁ</h1>
            </div>


        </div>
    </div>


    <!----VENDÉ----->
    <div id="fea" class="features">
        <div class="container">
            <div class="section-head text-center">
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 vender2">
                <h1>VENDER</h1>
            </div>
            <!----features-grids----->
            <div class="features-grids">
                <div class="col-md-4 col-sm-4 features-grid">
                    <div class="features-grid-info">

                        <div class="col-md-10 features-info">
                            <h3>¿CÓMO FUNCIONA?</h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>


                    <div class="col-md-10 features-info">
                        <h4>SELECCIÓN</h4>
                        <p>

                            Vení a la tienda con la ropa que ya no usas para que la seleccionemos y te abramos una
                            cuenta.</p>

                        <p>Vendemos tu ropa en <span>consignación. </span> A medida que las prendas se venden te pagamos
                            un <span>50%</span> por

                            las ropa de las marcas que valoramos, de temporada. Para las otras prendas pagamos un <span>33%</span>
                            y si

                            es ropa de niños un <span>40%.</span></p>

                        <p>La primer semana de cada mes te enviamos un mail para que sepas el crédito que tenés
                            juntado.</p>

                        <p>Cuando quieras podés usarlo para llevarte otras prendas o retirar el efectivo.</p>
                        </p>
                    </div>
                    <div class="clearfix"></div>

                    <div class="features-grid-info">

                        <div class="col-md-10 features-info">
                            <h4>TU CUENTA</h4>En Cocoliche vas a tener una cuenta a tu nombre y con tus datos,
                            detallando cada una de las

                            prendas que tomamos con un código que las identifique, así rastreamos sus movimientos y

                            sabemos cuándo está vendida.
                            <p></p>


                        </div>
                        <div class="clearfix"></div>
                    </div>


                </div><!---end-features-grid---->
                <div class="col-md-4 col-sm-4 features-grid text-center">
                    <div class="big-divice vender">
                        <img src="images/seleccion.png" title="features-demo"/>
                    </div>
                </div><!---end-features-grid---->
                <div class="col-md-4 col-sm-4 features-grid">
                    <div class="features-grid-info">

                        <div class="col-md-10 features-info espacio">
                            <h3>¿CÓMO TIENE QUE ESTAR LA ROPA QUE TRAÉS?</h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="features-grid-info">
                        <div class="col-md-10 features-info">
                            <h4>DE TEMPORADA</h4>
                            <p>La ropa debe ser de estación. De mujer,varón y niños de <strong>uso actual, moderna o
                                    buenos básicos.</strong> Si tenés algunas que pueden ser para la próxima temporda,
                                traelas más adelante.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="features-grid-info">
                        <div class="col-md-10 features-info">
                            <h4>EN BUEN ESTADO *</h4>
                            <p>La ropa que traigas debe estar en excelente estado, lavada y planchada.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="features-grid-info">
                        <div class="col-md-2 col-sm-2 features-icon">
                            <img class="warning" src="images/iconos/warning.png">
                        </div>
                        <div class="col-md-10 col-sm-10 features-info">
                            <h4>NO TOMAMOS</h4></div>
                        <div class="col-md-10 features-info">
                            <br>
                            <p>NO TOMAMOS abrigos de piel natural,
                                ropa interior, trajes, ropa deportiva.</p>
                            <p>*Importante: No recibimos prendas con desgaste de uso, roturas, manchas, modificaciones o
                                desperfectos, ni falsificaciones.</p>
                            <br>
                            <a href="#" class="scroll" data-toggle="modal" data-target=".bs-example-modal-lg2"><p><span>¿TE QUEDARON DUDAS?</span>
                                </p></a>
                            <br><br>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div><!---end-features-grid---->
                <div class="clearfix"></div>
            </div>
        </div>
        <!----//VENDÉ----->


        <!-----MARCAS QUE VALORAMOS---->
        <div class="section-head text-center">
            <!--<h3><span class="frist"> </span>MARCAS QUE VALORAMOS<span class="second"> </span></h3>
        </div>
        <div class="text-center"><p>Aunque tomamos cualquier tipo de ropa, estas son las marcas que más valoramos</p><br></div>
    --->
            <div class="featured">

                <script>
                    $(document).ready(function () {
                        $("#owl-demo2").owlCarousel({
                            items: 7,
                            lazyLoad: true,
                            autoPlay: true,
                            pagination: false,
                        });
                    });
                </script>

                <div id="owl-demo2" class="owl-carousel">
                    <div class="item">
                        <img src="images/brand-logo1.png" title="the verge"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo2.png" title="Mashable"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo3.png" title="TNW"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo4.png" title="bribble"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo5.png" title="the verge"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo6.png" title="the verge"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo7.png" title="Mashable"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo8.png" title="TNW"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo9.png" title="bribble"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo10.png" title="the verge"/>
                    </div>
                    <div class="item">
                        <img src="images/brand-logo11.png" title="the verge"/>
                    </div>

                </div>

            </div>
            <div class="todas">
                <a href="#" class="scroll" data-toggle="modal" data-target=".bs-example-modal-lg2">VER TODAS </a>
                <br><br>
            </div>
        </div>
        <!-----//MARCAS QUE VALORAMOS----->


        <!----MINICOCOLICHE----->
        <div id="mini" class="features">
            <div class="container">
                <div class="section-head text-center">
                    <div class="clearfix"></div>
                </div>
                <!----features-grids----->

                <div class="features-grids">
                    <div class="col-md-4 col-sm-4 features-grid">
                        <div class="features-grid-info">

                            <div class="col-md-10 features-info">
                                <div class="big-divice mini2">
                                    <img src="images/logo-mini2.png" title="features-demo"/>
                                </div>
                                <h3>¿CÓMO FUNCIONA?</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                        <div class="col-md-12 features-info">
                            <h4>SELECCIÓN</h4>
                            <p>Vení a la tienda con la ropa que ya no usas para que la seleccionemos y te abramos una
                                cuenta.</p>
                            <p>Vendemos tu ropa en <span>consignación.</span> A medida que las prendas se venden te
                                pagamos ​para la ropa de niños un <span>40%.</span></p>
                            <p>La primer semana de cada mes te enviamos un mail para que sepas el crédito que tenés
                                juntado.</p>
                            <p>Cuando quieras podés usarlo para llevarte otras prendas o retirar el efectivo.</p>

                        </div>
                        <div class="clearfix"></div>

                        <div class="features-grid-info">

                            <div class="col-md-12 features-info">
                                <h4>TU CUENTA</h4>En Cocoliche vas a tener una cuenta a tu nombre y con tus datos,
                                detallando cada una de las

                                prendas que tomamos con un código que las identifique, así rastreamos sus movimientos y

                                sabemos cuándo está vendida.
                                <p></p>

                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!---end-features-grid---->
                    <div class="col-md-4 col-sm-3 features-grid text-center">
                        <div class="big-divice mini">
                            <img src="images/logo-mini.png" title="features-demo"/>
                        </div>
                    </div><!---end-features-grid---->
                    <div class="col-md-4 col-sm-5 features-grid">
                        <div class="features-grid-info">

                            <div class="col-md-10 features-info espacio">
                                <h3>¿QUÉ SELECCIONAMOS?</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="features-grid-info">
                            <div class="col-md-12 features-info">
                                <h4>ROPA DE NIÑOS DE 1 A 10 AÑOS</h4>
                                <p>Actual y moderna. Las prendas que seleccionamos son, por ejemplo, camperas, sweaters,
                                    vestidos, blusas, camisas, sacos, pilotos, buzos, remeras, cardigans, jeans,
                                    etcétera. La ropa debe ser de estación.</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="features-grid-info">
                            <div class="col-md-12 features-info">
                                <h4>EN BUEN ESTADO *</h4>
                                <p>La ropa que traigas debe estar en excelente estado, lavada y planchada.</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="features-grid-info">
                            <div class="col-md-2 col-sm-2 features-icon">
                                <img class="warning" src="images/iconos/warning.png">
                            </div>
                            <div class="col-md-10 col-sm-10 features-info">
                                <h4>NO TOMAMOS</h4></div>
                            <div class="col-md-10 features-info">
                                <br>
                                <p>NO TOMAMOS ropa de bebés, calzado ni accesorios.</p>
                                <p>*Importante: No recibimos prendas con desgaste de uso, roturas, manchas,
                                    modificaciones o desperfectos, ni falsificaciones.</p>
                                <br>
                                <a href="#" class="scroll" data-toggle="modal" data-target=".bs-example-modal-lg3"><p>
                                        <span>¿TE QUEDARON DUDAS?</span></p></a>
                                <br><br>
                            </div>

                            <div class="clearfix"></div>
                        </div>


                    </div><!---end-features-grid---->
                    <div class="features-grid-info col-md-12 col-sm-12 col-xs-12 ">

                        <div class="features-info">
                            <h4 style="text-align: center">¿CUÁLES SON LAS MARCAS QUE TOMAMOS?</h4>
                            <p><span>Para niñ@s tomamos principalmente ropa de estas marcas: </span>47 Street Kids,
                                Adidas Kids, Atomik, Baby Cottons, Banana Republic Kids, Benetton, Carter’s, Cheeky,
                                Coniglio, Crocs Kids, Disney, Gap Kids, Grisino, Gymboree, H&M Kids, Hello Kitty,
                                Kevingston, Kosiuko, La Jolie, Levi’s, Little Akiabara, Magdalena Esposito, Mimo & Co.,
                                Mini Complot, Montagne, Nike Kids, Old Navy Kids, Ona Saez Kids, Oshkosh, Owoko, Paula
                                Cahen D’Anvers Kids, Pecosos, Pioppa, Polo Kids, Prénatal, Primera Huella, Ralph Lauren,
                                Rapsodia Kids, Tommy Hilfiger Kids, Topper Kids, Vans Kids, Zara Kids.
                            </p>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!----//VENDÉ----->
            <div class="banda2 text-center" id="blog">
                <div class="container text-center">
                    <div class="col-md-12 blog text-center">
                        <h1>BLOG</h1>
                    </div>


                </div>
            </div>

            <!--Blog-->
            <div id="gallery" class="screen-shot-gallery">

                <div class="sreen-gallery-cursual">
                    <link href="css/owl.carousel.css" rel="stylesheet">
                    <script src="js/owl.carousel.js"></script>
                    <script>
                        $(document).ready(function () {
                            $("#owl-demo").owlCarousel({
                                items: 3,
                                lazyLoad: true,
                                autoPlay: true,
                            });
                        });
                    </script>

                    <div class="container">
                        <div id="owl-demo" class="owl-carousel">
                            <?php foreach ($dbManager->getSliderItems() as $sliderItem): ?>
                                <div class="item">
                                    <h3><?= $sliderItem['titulo'] ?> </h3>
                                    <img src="<?= DIR_WS_IMAGES_BLOG_THUMB . $sliderItem['imagen'] ?>"
                                         class="img-responsive">
                                    <p><?= substr(strip_tags($sliderItem['copete']), 0, 300) ?></p>
                                    <a href="#" data-toggle="modal" data-target=".blognotas"
                                       data-Id="<?= $sliderItem['Id'] ?>"
                                       data-imagen="<?= DIR_WS_IMAGES_BLOG_THUMB . $sliderItem['imagen'] ?>">
                                        LEER +</a>
                                </div>
                            <?php endforeach; ?>
                            <!--<div class="item">
                                <h3>COCOLICHE un estilo de vida </h3>
                                <img src="images/valija.jpg" class="img-responsive">
                                <p>Como vos, hay muchas personas que se acercan a la tienda con idea de alivianar su
                                    equipaje y liberar su ropa del placard permitiendo que otros puedan reutilizarla,
                                    por eso decimos que somos una comunidad de ropa con otra oportunidad.</p>
                                <a href="#" data-toggle="modal" data-target=".blognotas">LEER +</a>
                            </div>
                            <div class="item">
                                <h3>Sumate a la moda sustentable</h3>
                                <img src="images/03.jpg" class="img-responsive">
                                <p>Comprá ropa de las marcas que te gustan, a precios accesibles, sin sumar desperdicios
                                    al mundo</p>
                                <a href="#">LEER +</a>
                            </div>
                            <div class="item">
                                <h3>COCOCLICHE un estilo de vida </h3>
                                <img src="images/valija.jpg" class="img-responsive">
                                <p>Como vos, hay muchas personas que se acercan a la tienda con idea de alivianar su
                                    equipaje y liberar su ropa del placard permitiendo que otros puedan reutilizarla,
                                    por eso decimos que somos una comunidad de ropa con otra oportunidad.</p>
                                <a href="#">LEER +</a>
                            </div>
                            <div class="item">
                                <h3>COCOCLICHE un estilo de vida </h3>
                                <img src="images/valija.jpg" class="img-responsive">
                                <p>Como vos, hay muchas personas que se acercan a la tienda con idea de alivianar su
                                    equipaje y liberar su ropa del placard permitiendo que otros puedan reutilizarla,
                                    por eso decimos que somos una comunidad de ropa con otra oportunidad.</p>
                                <a href="#">LEER +</a>
                            </div>
                            <div class="item">
                                <h3>COCOCLICHE un estilo de vida </h3>
                                <img src="images/valija.jpg" class="img-responsive">
                                <p>Como vos, hay muchas personas que se acercan a la tienda con idea de alivianar su
                                    equipaje y liberar su ropa del placard permitiendo que otros puedan reutilizarla,
                                    por eso decimos que somos una comunidad de ropa con otra oportunidad.</p>
                                <a href="#">LEER +</a>
                            </div>
                            <div class="item">
                                <h3>COCOCLICHE un estilo de vida </h3>
                                <img src="images/valija.jpg" class="img-responsive">
                                <p>Como vos, hay muchas personas que se acercan a la tienda con idea de alivianar su
                                    equipaje y liberar su ropa del placard permitiendo que otros puedan reutilizarla,
                                    por eso decimos que somos una comunidad de ropa con otra oportunidad.</p>
                                <a href="#">LEER +</a>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <!--//Blog--->

            <!-----consulta---->
            <div id="contact" class="getintouch">
                <div class="container">
                    <div class="section-head text	-center">
                        <h3><span class="frist"> </span>TU CONSULTA<span class="second"> </span></h3>
                    </div>
                    <!---->
                    <div class="col-md-9 getintouch-left">
                        <div class="contact-form col-md-10">
                            <h3>Hola!</h3>
                            <form id="contact-form" method="post">
                                <input id="nombre" type="text" name="nombre" placeholder="Nombre" required/>
                                <input id="email" type="text" name="email" placeholder="Email" required/>
                                <textarea id="mensaje" name="mensaje" placeholder="Mensaje" required/></textarea>
                                <input id="submit" type="submit" name="submit" value="Enviar"/>
                            </form>
                        </div>
                        <ul class="footer-social-icons col-md-2 text-center">
                            <li><a href="https://www.facebook.com/CocolicheRopaconotraOportunidad/" target="_blank"> <i
                                        class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/explore/locations/1022363353/" target="_blank"><i
                                        class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <div class="clearfix"></div>
                        </ul>
                    </div>
                    <div class="col-md-2 getintouch-left">
                        <div class="footer-divice">
                            <img src="images/divice-half.png" title="getintouch"/>
                        </div>
                    </div>
                </div>
            </div>
            <!---//consulta->

            <!-footer-->
            <div class="footer">
                <div class="container">
                    <div class="footer-grids">

                        <div class="col-md-4 col-sm-4 col-xs-12 footer-grid about-info">
                            <a href="#"><img style="width: 28%" src="images/logo-image.png" title="Cocoliche"/></a> <img
                                style="padding-top: 10px" src="images/oportunidad.png">
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 footer-grid subscribe text-center">
                            <p>Diseño<a href="http://estudioumo.com.ar" target="_blank"> <img src="images/umo.png"></a>
                            </p>
                            <br><br>
                        </div>


                        <div class="col-md-4 col-sm-4 col-xs-12 pull-right" style="border-left: solid 3px #000">
                            <p>
                                <?= $configuration['Empresa_Direccion'] ?> <br> (Casi esquina 11 y 41)
                                <?= $configuration['Empresa_Localidad'] ?></p>

                            <ul class="footer-icons">
                                <li><a href="<?= $configuration['Url_Facebook'] ?>" target="_blank">
                                        <i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="<?= $configuration['Url_Instagram'] ?>" target="_blank"><i
                                            class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <div class="clearfix"></div>
                            </ul>

                        </div>

                        <div class="clearfix"></div>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                /*
                                 var defaults = {
                                 containerID: 'toTop', // fading element id
                                 containerHoverID: 'toTopHover', // fading element hover id
                                 scrollSpeed: 1200,
                                 easingType: 'linear'
                                 };
                                 */

                                $().UItoTop({easingType: 'easeOutQuart'});

                            });
                        </script>
                        <a href="#" id="toTop" style="display: block;"> <span id="toTopHover"
                                                                              style="opacity: 1;"> </span></a>
                    </div>
                </div>
            </div>
            <div class="modal fade" tabindex="-1" role="dialog" id="send-mail-modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default dismiss-contact-modal hidden" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
</body>
</html>

