$(document).ready(function () {
    $('#contact-form').submit(function (e) {
        e.preventDefault()
        $.ajax({
            async: true,
            url: 'php/send_mail.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            beforeSend: function (response) {
                $('#send-mail-modal').modal('show').find('.modal-body').html('Enviando mensaje...')
            },
            success: function (response) {
                $('#send-mail-modal').find('.modal-body').html(response.msg)
                $('#dismiss-contact-modal').removeClass('hidden');
            }
        });
    })
    $('#send-mail-modal').modal({
        keyboard: false,
        backdrop: false,
        show: false,
    })
    $('#send-mail-modal').on('hidden.bs.modal', function (e) {
        location.reload();
    })
})