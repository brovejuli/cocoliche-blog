$(document).ready(function () {
    $('.blognotas').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        var imagen = button.data('imagen') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        $.ajax({
            async: true,
            url: 'php/modal.php',
            type: 'POST',
            data: {
                Id: id,
            },
            dataType: 'json',
            beforeSend: function (response) {
            },
            success: function (response) {
                $('.titulo').text(response.titulo)
                $('.copete').html(response.copete)
                $('.cuerpo').html(response.cuerpo)
                $('.imagen').attr('src', imagen)
            }
        });

    })
})